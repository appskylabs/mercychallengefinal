//
//  Step3ViewController.h
//  Mercy Challenge
//
//  Created by Taylor Korensky on 4/29/16.
//  Copyright © 2016 Appsky Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MemoryListViewController.h"
#import "AppDelegate.h"

@interface Step3ViewController : UIViewController
- (IBAction)reRankClicked:(id)sender;
- (IBAction)finishClicked:(id)sender;

@end
