//
//  RightMenuViewController.h
//  1Mind->1Heart
//
//  Created by Taylor Korensky on 4/22/16.
//  Copyright © 2016 Appsky Labs. All rights reserved.
//

#import "Book.h"
#import "AppDelegate.h"
#import "Challenge1RootViewController.h"
#import <UIKit/UIKit.h>
#import "Chapters.h"
#import "Challenge2ShareViewController.h"
#import "Challenge3ViewController.h"
#import "SKSTableView.h"
#import "SKSTableViewCell.h"
#import "UIHomePageViewController.h"

@interface RightMenuViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, SKSTableViewDelegate>
@property (strong, nonatomic) IBOutlet SKSTableView *tableView;
@property (strong, nonatomic) NSMutableArray *chaptersArray;
@property (strong, nonatomic) NSMutableArray *finalArray;
@property (strong, nonatomic) Book *book;
@property int highlightCounter;
- (IBAction)collapseList:(id)sender;

@end
