//
//  Challenge3ViewController.h
//  Mercy Challenge
//
//  Created by Taylor Korensky on 5/2/16.
//  Copyright © 2016 Appsky Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface Challenge3ViewController : UIViewController
- (IBAction)nextButtonClicked:(id)sender;
- (IBAction)nextClicked:(id)sender;

@end
