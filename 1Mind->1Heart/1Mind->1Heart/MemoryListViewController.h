//
//  MemoryListViewController.h
//  Mercy Challenge
//
//  Created by Taylor Korensky on 4/25/16.
//  Copyright © 2016 Appsky Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OpinionzAlertView.h"
#import "Backendless.h"
#import "Memory.h"
#import "MemoryViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "AppDelegate.h"


@interface MemoryListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) NSMutableArray *memoryArray;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)addMemoryClicked:(id)sender;


@end
