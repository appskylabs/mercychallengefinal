//
//  GoingForwardViewController.h
//  Mercy Challenge
//
//  Created by Taylor Korensky on 4/29/16.
//  Copyright © 2016 Appsky Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OpinionzAlertView.h"
#import "AppDelegate.h"
#import "Challenge2ShareViewController.h"
#import "AppDelegate.h"

@interface GoingForwardViewController : UIViewController <UITextViewDelegate>

- (IBAction)goHomeClicked:(id)sender;
- (IBAction)nextChallenge:(id)sender;
- (IBAction)nextSwipe:(id)sender;
@property (strong, nonatomic) IBOutlet UITextView *textView;
@end
