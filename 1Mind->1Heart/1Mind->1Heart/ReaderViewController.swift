//
//  ReaderViewController.swift
//  1Mind->1Heart
//
//  Created by Taylor Korensky on 3/28/16.
//  Copyright © 2016 Appsky Labs. All rights reserved.
//

import UIKit
import FolioReaderKit

@objc class ReaderViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func open() {
        let config = FolioReaderConfig()
        let bookPath = NSBundle.mainBundle().pathForResource("1459200237", ofType: "epub")
        FolioReader.presentReader(parentViewController: self, withEpubPath: bookPath!, andConfig: config)
    }

}
