//
//  LeftMenuViewController.m
//  
//
//  Created by Taylor Korensky on 4/21/16.
//
//

#import "LeftMenuViewController.h"

@interface LeftMenuViewController ()

@end

@implementation LeftMenuViewController

- (void)viewDidLoad {

    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.titlesArray = [[NSMutableArray alloc] init];
    
    [self.titlesArray addObject:@"Home"];
   // [self.titlesArray addObject:@"Mercy Challenge Book"];
    [self.titlesArray addObject:@"Healing Memories"];
    [self.titlesArray addObject:@"Contact Us"];
    [self.titlesArray addObject:@"Donate"];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if( cell == nil ) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    [cell.textLabel setTextColor:[UIColor whiteColor]];
    [cell.textLabel setFont:[UIFont fontWithName:@"HelveticaNeue-bold" size:18]];
    [cell.textLabel setText:[self.titlesArray objectAtIndex:indexPath.row]];
    
    return cell;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.titlesArray.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
     AppDelegate *appDelegate = [AppDelegate sharedInstanceofAppdelegate];
    if(indexPath.row == 0){
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"LandingPageStoryboard" bundle:nil];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[storyboard instantiateViewControllerWithIdentifier:@"landingPage"]];
        [appDelegate.sideMenuViewController setContentViewController:navigationController animated:YES];
    }
    else if(indexPath.row == 1){
       
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"HomePageStoryboard" bundle:nil];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[storyboard instantiateViewControllerWithIdentifier:@"homePage"]];
        [appDelegate.sideMenuViewController setContentViewController:navigationController animated:YES];

        
    }
    else if(indexPath.row == 2){
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ContactUsStoryboard" bundle:nil];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[storyboard instantiateViewControllerWithIdentifier:@"contactUsView"]];
        [appDelegate.sideMenuViewController setContentViewController:navigationController animated:YES];
      

    }
    else if(indexPath.row == 3){
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"DonateStoryboard" bundle:nil];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[storyboard instantiateViewControllerWithIdentifier:@"donateView"]];
        [appDelegate.sideMenuViewController setContentViewController:navigationController animated:YES];

        
    }
      [appDelegate.sideMenuViewController hideMenuViewController];
}




@end
