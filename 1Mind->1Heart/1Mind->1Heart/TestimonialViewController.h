//
//  TestimonialViewController.h
//  Mercy Challenge
//
//  Created by Taylor Korensky on 5/2/16.
//  Copyright © 2016 Appsky Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "Challenge3ViewController.h"
#import "Testimonial.h"

@interface TestimonialViewController : UIViewController <UITextViewDelegate>
@property (strong, nonatomic) IBOutlet UITextView *textView;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentController;
- (IBAction)nextChallengeClicked:(id)sender;
- (IBAction)goHomeClicked:(id)sender;
- (IBAction)submitTestimonyClicked:(id)sender;
- (IBAction)goToNextChallenge:(id)sender;

@end
