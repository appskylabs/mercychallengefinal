//
//  Challenge1RootViewController.h
//  Mercy Challenge
//
//  Created by Taylor Korensky on 4/23/16.
//  Copyright © 2016 Appsky Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface Challenge1RootViewController : UIViewController

- (IBAction)swipeRight:(id)sender;

@end
