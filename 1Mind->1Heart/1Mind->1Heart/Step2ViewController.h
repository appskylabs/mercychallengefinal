//
//  Step2ViewController.h
//  Mercy Challenge
//
//  Created by Taylor Korensky on 4/29/16.
//  Copyright © 2016 Appsky Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MemoryListViewController.h"
#import "AppDelegate.h"

@interface Step2ViewController : UIViewController <UITextViewDelegate>
- (IBAction)reRankClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UITextView *textView;

@end
