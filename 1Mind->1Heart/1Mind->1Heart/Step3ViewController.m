//
//  Step3ViewController.m
//  Mercy Challenge
//
//  Created by Taylor Korensky on 4/29/16.
//  Copyright © 2016 Appsky Labs. All rights reserved.
//

#import "Step3ViewController.h"

@interface Step3ViewController ()

@end

@implementation Step3ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpBackgroundImage];
    // Do any additional setup after loading the view.
    UIButton *rightButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [rightButton setBackgroundImage: [UIImage imageNamed: @"BookIcon"] forState:UIControlStateNormal];
    UIBarButtonItem *bookButton = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    [rightButton addTarget:self action:@selector(openRightMenu) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = bookButton;
   
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goToChapter:) name:@"GoToChapterFromOtherView" object:nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) setUpBackgroundImage{
    
    UIImageView *background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [background setImage:[UIImage imageNamed:@"sail7"]];
    [background setContentMode:UIViewContentModeScaleAspectFill];
    [background setAlpha:0.75];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [view setBackgroundColor:[UIColor blackColor]];
    [view setAlpha: 0.3];
    [self.view addSubview:view];
    [self.view sendSubviewToBack:view];
    [self.view addSubview:background];
    [self.view sendSubviewToBack:background];
}


- (IBAction)reRankClicked:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Challenge1Storyboard" bundle:nil];
    MemoryListViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"memoryListView"];
    [self showViewController:controller sender:self];

    
}
-(void) openRightMenu {
    
    AppDelegate *appDelegate = [AppDelegate sharedInstanceofAppdelegate];
    [appDelegate.sideMenuViewController presentRightMenuViewController];
    
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"HighlightChapter" object:[NSNumber numberWithInt:(int)[appDelegate.book.chapters count] - 1] userInfo:nil];
}

-(void) goToChapter: (NSNotification *) notification{
    
    NSArray *array = [[notification object] componentsSeparatedByString:@","];
    NSLog(@"Array: %@", array);
    __block int counter = [array[0] intValue];
    
    AppDelegate *appDelegate = [AppDelegate sharedInstanceofAppdelegate];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"HomePageStoryboard" bundle:nil];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[storyboard instantiateViewControllerWithIdentifier:@"homePage"]];
    
    [appDelegate.sideMenuViewController setContentViewController:navigationController animated:YES];
    
    [appDelegate.sideMenuViewController hideMenuViewController];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"GoToChapter" object:[NSNumber numberWithInt:counter] userInfo:nil];
    });}



@end
