//
//  ChallengeTrackerViewController.h
//  Mercy Challenge
//
//  Created by Taylor Korensky on 5/2/16.
//  Copyright © 2016 Appsky Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OpinionzAlertView.h"
#import "Backendless.h"
#import "TestimonialViewController.h"
#import "MercyTracker.h"

@interface ChallengeTrackerViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
- (IBAction)submitClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *hearAboutTextField;
@property (strong, nonatomic) IBOutlet UITextField *impactOnLifeTextField;
@property (strong, nonatomic) IBOutlet UITextField *dioceseTextField;
@property (strong, nonatomic) IBOutlet UITextField *countryTextField;
@property (strong, nonatomic) IBOutlet UITextField *howManyForgivenTextField;
@property (strong, nonatomic) IBOutlet UITextField *hurtsHealed;
@property (strong, nonatomic) IBOutlet UITextField *howManyChallengedTextField;
@property (strong, nonatomic) IBOutlet UITextField *feedbackTextField;
- (IBAction)skipButtonClicked:(id)sender;

@end
