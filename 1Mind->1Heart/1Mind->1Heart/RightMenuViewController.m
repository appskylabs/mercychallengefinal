//
//  RightMenuViewController.m
//  1Mind->1Heart
//
//  Created by Taylor Korensky on 4/22/16.
//  Copyright © 2016 Appsky Labs. All rights reserved.
//

#import "RightMenuViewController.h"

@interface RightMenuViewController ()

@end

@implementation RightMenuViewController

- (void)viewDidLoad {
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self.tableView setBackgroundColor:[UIColor clearColor]];
    AppDelegate *appDelegate = [AppDelegate sharedInstanceofAppdelegate];
    self.chaptersArray = [[NSMutableArray alloc] init];
    self.finalArray = [[NSMutableArray alloc] init];
    self.book = [[Book alloc] init];
    self.book = appDelegate.book;
    self.chaptersArray  = self.book.chapters;
    int currentHeaderIndex = 0;

    
    NSLog(@"Chapters Array: %@", self.chaptersArray);
    
    for(int x = 0; x < self.chaptersArray.count; x++){
        
        
        NSMutableArray *temp = [[NSMutableArray alloc] init];
        if([[[self.chaptersArray objectAtIndex:x] subheader] intValue] == 0){ //Main header
            
            [temp addObject:[self.chaptersArray objectAtIndex:x]];
            [self.finalArray addObject:temp];
             currentHeaderIndex = (int)self.finalArray.count - 1;
        }
        else{
            
            [[self.finalArray objectAtIndex:currentHeaderIndex] addObject:[self.chaptersArray objectAtIndex:x]];
        }
    }
    
    NSLog(@"Final Chapters Array: %@", self.finalArray);
    
    [self.finalArray addObject:@[@"Challenge #1"]];
    [self.finalArray addObject:@[@"Memory Journal"]];
    [self.finalArray addObject:@[@"Challenge #2"]];
    [self.finalArray addObject:@[@"Challenge #3"]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(highlightChapter:) name:@"HighlightChapter" object:nil];
    
    self.tableView.SKSTableViewDelegate = self;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if( cell == nil ) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    
    if(indexPath.row == self.highlightCounter){
        
        [cell.contentView setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.35]];
    }
    else{
        
        [cell.contentView setBackgroundColor:[UIColor clearColor]];
    }
    
    
    [cell.textLabel setTextColor:[UIColor whiteColor]];
    
    
    [cell.textLabel setNumberOfLines:2];
    [cell.textLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [cell.textLabel setTextAlignment:NSTextAlignmentRight];
    
    if([[self.chaptersArray objectAtIndex:indexPath.row] isKindOfClass:[NSString class]]){
    
        
        [cell.textLabel setFont:[UIFont fontWithName:@"HelveticaNeue-bold" size:15]];
        [cell.textLabel setText:[self.chaptersArray objectAtIndex:indexPath.row]];
    }
    else{
    
        if([[[self.chaptersArray objectAtIndex:indexPath.row] subheader] intValue] == 0){
            [cell.textLabel setFont:[UIFont fontWithName:@"HelveticaNeue-bold" size:15]];
             [cell.textLabel setText:[NSString stringWithFormat:@"%@", [[self.chaptersArray objectAtIndex:indexPath.row] title]]];
        }
        else{
            [cell.textLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:11]];
             [cell.textLabel setText:[NSString stringWithFormat:@"\t\t\t\t%@", [[self.chaptersArray objectAtIndex:indexPath.row] title]]];

        }
        
        
    }
    
    
    
    
    return cell;
    
}*/


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.finalArray count];
}

- (NSInteger)tableView:(SKSTableView *)tableView numberOfSubRowsAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.finalArray[indexPath.row] count] - 1;
}

- (BOOL)tableView:(SKSTableView *)tableView shouldExpandSubRowsOfCellAtIndexPath:(NSIndexPath *)indexPath
{
   /* if ([self.finalArray[indexPath.row] count] > 1)
    {
        return YES;
    }*/
    
    return NO;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SKSTableViewCell";
    
    SKSTableViewCell *cell = (SKSTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
        cell = [[SKSTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
   /* if(indexPath.row == self.highlightCounter){
        
        [cell setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.35]];
    }
    else{
        
        [cell setBackgroundColor:[UIColor clearColor]];
    }*/
    [cell setBackgroundColor:[UIColor clearColor]];
    
    [cell.textLabel setTextColor:[UIColor whiteColor]];
    
    
    [cell.textLabel setNumberOfLines:3];
    [cell.textLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [cell.textLabel setTextAlignment:NSTextAlignmentRight];
    [cell.textLabel setFont:[UIFont fontWithName:@"HelveticaNeue-bold" size:16]];
    
    if([[self.finalArray objectAtIndex:indexPath.row][0] isKindOfClass:[NSString class]]){
        [cell.textLabel setText:[self.finalArray objectAtIndex:indexPath.row][0]];
    }
    else{
         [cell.textLabel setText:[NSString stringWithFormat:@"%@", [[self.finalArray objectAtIndex:indexPath.row][0] title]]];
    }
  
    
    if([self.finalArray[indexPath.row] count] > 1)
        cell.expandable = YES;
    else
        cell.expandable = NO;
    
    return cell;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForSubRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"UITableViewCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
     [cell setBackgroundColor:[UIColor clearColor]];
    
   /* if(indexPath.row == self.highlightCounter){
        
        [cell setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.35]];
    }
    else{
        
       
    }*/
    
    [cell.textLabel setTextColor:[UIColor whiteColor]];
    [cell.textLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:13]];
    [cell.textLabel setNumberOfLines:2];
    [cell.textLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [cell.textLabel setTextAlignment:NSTextAlignmentRight];

    [cell.textLabel setText:[NSString stringWithFormat:@"%@", [[self.finalArray objectAtIndex:indexPath.row][indexPath.subRow] title]]];
    
    return cell;
}

- (CGFloat)tableView:(SKSTableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Section: %ld, Row:%ld, Subrow:%ld", (long)indexPath.section, (long)indexPath.row, (long)indexPath.subRow);
    AppDelegate *appDelegate = [AppDelegate sharedInstanceofAppdelegate];
    if([[self.finalArray objectAtIndex:indexPath.row][0] isKindOfClass:[NSString class]]){
        
        if(indexPath.row == 11){
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Challenge1Storyboard" bundle:nil];
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[storyboard instantiateViewControllerWithIdentifier:@"challenge1Root"]];
            [appDelegate.sideMenuViewController setContentViewController:navigationController];
        }
        else if(indexPath.row == 12){
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Challenge1Storyboard" bundle:nil];
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[storyboard instantiateViewControllerWithIdentifier:@"memoryListView"]];
            [appDelegate.sideMenuViewController setContentViewController:navigationController];
        }
        else if(indexPath.row == 13){
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Challenge2Storyboard" bundle:nil];
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[storyboard instantiateViewControllerWithIdentifier:@"challenge2Root"]];
            [appDelegate.sideMenuViewController setContentViewController:navigationController];
        }
        else if(indexPath.row == 14){
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Challenge3Storyboard" bundle:nil];
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[storyboard instantiateViewControllerWithIdentifier:@"challenge3Root"]];
            [appDelegate.sideMenuViewController setContentViewController:navigationController];
            
        }
        
    }else{
        
        if([appDelegate.sideMenuViewController.contentViewController.title isEqualToString:@"Mercy Challenge"]){

        [[NSNotificationCenter defaultCenter] postNotificationName:@"GoToChapter" object:[NSNumber numberWithInt:(int)[self.chaptersArray indexOfObjectIdenticalTo:[self.finalArray objectAtIndex:indexPath.row][0]]] userInfo:nil];
            
        }
        else{
        
            BOOL headerOrNot = NO;
            if([[self.finalArray objectAtIndex:indexPath.row] count] > 1){
                headerOrNot = NO;
            }
            else{
                headerOrNot = YES;
            }

            [[NSNotificationCenter defaultCenter] postNotificationName:@"GoToChapterFromOtherView" object:[NSString stringWithFormat:@"%@,%d",[NSNumber numberWithInt:(int)[self.chaptersArray indexOfObjectIdenticalTo:[self.finalArray objectAtIndex:indexPath.row][0]]], headerOrNot] userInfo:nil];

        }
    }
    
    if([[self.finalArray objectAtIndex:indexPath.row] count] > 1){
        
    }
    else{
        [appDelegate.sideMenuViewController hideMenuViewController];
    }
    
}

- (void)tableView:(SKSTableView *)tableView didSelectSubRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Section: %ld, Row:%ld, Subrow:%ld", (long)indexPath.section, (long)indexPath.row, (long)indexPath.subRow);
    AppDelegate *appDelegate = [AppDelegate sharedInstanceofAppdelegate];

    if([appDelegate.sideMenuViewController.contentViewController.title isEqualToString:@"Mercy Challenge"]){
       
        [[NSNotificationCenter defaultCenter] postNotificationName:@"GoToChapter" object:[NSNumber numberWithInt:(int)[self.chaptersArray indexOfObjectIdenticalTo:[self.finalArray objectAtIndex:indexPath.row][indexPath.subRow]]] userInfo:nil];
        [appDelegate.sideMenuViewController hideMenuViewController];
    }
    else{
    
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"GoToChapterFromOtherView" object:[NSString stringWithFormat:@"%@,%d",[NSNumber numberWithInt:(int)[self.chaptersArray indexOfObjectIdenticalTo:[self.finalArray objectAtIndex:indexPath.row][indexPath.subRow]]], NO] userInfo:nil];
    }
    
}


/*-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    AppDelegate *appDelegate = [AppDelegate sharedInstanceofAppdelegate];
    if([[self.chaptersArray objectAtIndex:indexPath.row] isKindOfClass:[NSString class]]){
        
        if(indexPath.row == 29){
            
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Challenge1Storyboard" bundle:nil];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[storyboard instantiateViewControllerWithIdentifier:@"challenge1Root"]];
        [appDelegate.sideMenuViewController setContentViewController:navigationController];
        }
        else if(indexPath.row == 30){
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Challenge2Storyboard" bundle:nil];
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[storyboard instantiateViewControllerWithIdentifier:@"challenge2Root"]];
            [appDelegate.sideMenuViewController setContentViewController:navigationController];
        }
        else if(indexPath.row == 31){
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Challenge3Storyboard" bundle:nil];
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[storyboard instantiateViewControllerWithIdentifier:@"challenge3Root"]];
            [appDelegate.sideMenuViewController setContentViewController:navigationController];
            
        }
        
    }else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"GoToChapter" object:[NSNumber numberWithInt:(int)indexPath.row] userInfo:nil];
    }
    
    [appDelegate.sideMenuViewController hideMenuViewController];
    
}*/


-(void) highlightChapter: (NSNotification *) notification{
    
    self.highlightCounter = [[notification object] intValue];
    [self.tableView reloadData];
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.highlightCounter inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (IBAction)collapseList:(id)sender {
    
        [self.tableView collapseCurrentlyExpandedIndexPaths];
    
}
@end
