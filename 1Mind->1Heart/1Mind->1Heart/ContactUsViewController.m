//
//  ContactUsViewController.m
//  1Mind->1Heart
//
//  Created by Taylor Korensky on 3/30/16.
//  Copyright © 2016 Appsky Labs. All rights reserved.
//

#import "ContactUsViewController.h"

@interface ContactUsViewController ()

@end

@implementation ContactUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIColor *patternColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]];
    [self.backgroundView setBackgroundColor:patternColor];
    self.logoView.layer.borderWidth = 4.0f;
    self.logoView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.messageTextView.delegate = self;
    self.messageTextView.layer.cornerRadius = 8.0f;
    self.isShowingMenu = NO;
    //[self setUpMenu];
    
    UIButton *aButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [aButton setBackgroundImage: [UIImage imageNamed: @"MenuSettings"] forState:UIControlStateNormal];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:aButton];
    [aButton addTarget:self action:@selector(openMenu) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = backButton;
    self.navigationItem.title = @"Contact Us";
    
    UIButton *rightButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [rightButton setBackgroundImage: [UIImage imageNamed: @"BookIcon"] forState:UIControlStateNormal];
    UIBarButtonItem *bookButton = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    [rightButton addTarget:self action:@selector(openRightMenu) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = bookButton;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goToChapter:) name:@"GoToChapterFromOtherView" object:nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*-(void) setUpMenu {
    
    
    NSMutableArray *menuItems = [[NSMutableArray alloc] init];
    REMenuItem *homeItem = [[REMenuItem alloc] initWithTitle:@"Mercy Challenge"
                                                       image:nil
                                            highlightedImage:nil
                                                      action:^(REMenuItem *item) {
                                                          
                                                          [self.menu close];
                                                          self.isShowingMenu = NO;
                                                          UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"HomePageStoryboard" bundle:nil];
                                                          UIHomePageViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"homePage"];
                                                          
                                                          [self presentViewController: viewController animated:YES completion:nil];
                                                      }];
    [menuItems addObject:homeItem];
    
    REMenuItem *donateItem = [[REMenuItem alloc] initWithTitle:@"Donate"
                                                         image:nil
                                              highlightedImage:nil
                                                        action:^(REMenuItem *item) {
                                                            
                                                            [self.menu close];
                                                            self.isShowingMenu = NO;
                                                            
                                                            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"DonateStoryboard" bundle:nil];
                                                            DonateViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"donateView"];
                                                            [self presentViewController: viewController animated:YES completion:nil];
                                                            
                                                            
                                                        }];
    [menuItems addObject:donateItem];

    
    
    REMenuItem *logoutItem = [[REMenuItem alloc] initWithTitle:@"Logout"
                                                         image:nil
                                              highlightedImage:nil
                                                        action:^(REMenuItem *item) {
                                                            
                                                            
                                                            [self.menu close];
                                                            self.isShowingMenu = NO;
                                                            [self logout];
                                                        }];
    [menuItems addObject:logoutItem];
    
    
    
    self.menu = [[REMenu alloc] initWithItems:menuItems];
    
    [self.menu setTextColor:[UIColor colorWithRed:0.9568 green:0.7788 blue:0.1842 alpha:1.0]];
    [self.menu setBackgroundAlpha:0.7f];
}



- (IBAction)menuClicked:(id)sender {
    
    if(self.isShowingMenu != YES){
        
        [self.menu showFromRect:CGRectMake(0, 72, 420, 300) inView:self.view];
        self.isShowingMenu = YES;
    }
    else{
        
        
        [self.menu close];
        self.isShowingMenu = NO;
    }
    
}*/

- (IBAction)facebookClicked:(id)sender {
    
      [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.facebook.com/HolyFamilyCounseling/"]];
}

- (IBAction)twitterClicked:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://twitter.com/CatholicCounslr"]];
}

- (IBAction)websiteClicked:(id)sender {
    
      [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://holyfamilycounseling.org"]];
}


- (IBAction)callToAction:(id)sender {
    
    //Send email with content
    if(self.nameTextfield.text.length > 0 && self.subjectTextfield.text > 0 && self.messageTextView.text.length > 0 && ![self.messageTextView.text isEqualToString:@"I am seeking counseling for: "]){
        
        [self.submitButton setEnabled:NO];
        [SVProgressHUD show];
    
        NSString *subject = [NSString stringWithFormat:@"Counseling Inquiry from Mercy Challenge App"];
        NSString *body = [NSString stringWithFormat:@"Phone Number: %@\nTimeZone: %@\n%@", self.nameTextfield.text, self.subjectTextfield.text, self.messageTextView.text];
        
        NSString *recipient = @"ibutler@holyfamilycounseling.org";
       // NSString *recipient = @"taylor@appskylabs.com";
        
        [backendless.messagingService sendTextEmail:subject body:body to:@[recipient] response:^(id response)  {
            
            [self.submitButton setEnabled:YES];
            self.nameTextfield.text = @"";
            self.subjectTextfield.text = @"";
            self.messageTextView.text = @"I am seeking counseling for: ";
            NSLog(@"Email has been sent");
            [SVProgressHUD dismiss];
            
            
            OpinionzAlertView *alertView = [[OpinionzAlertView alloc] initWithTitle:@"Success" message: @"Your counseling inquiry has been sent. Holy Family Counseling will contact you soon." cancelButtonTitle:@"Okay" otherButtonTitles:nil];
            [alertView setIconType:OpinionzAlertIconSuccess];
            [alertView show];


            
        } error:^(Fault *fault) {
            NSLog(@"Server reported an error: %@", fault);
            
            [self.submitButton setEnabled:YES];
            [SVProgressHUD dismiss];
            
            OpinionzAlertView *alertView = [[OpinionzAlertView alloc] initWithTitle:@"Error" message: @"Error sending the message.  Please try again later." cancelButtonTitle:@"Okay" otherButtonTitles:nil];
            [alertView setIconType:OpinionzAlertIconWarning];
            [alertView show];

        }];
        
    }
    else{
        
        OpinionzAlertView *alertView = [[OpinionzAlertView alloc] initWithTitle:@"Missing Info" message: @"Please make sure all fields are fileld out before submitting a message." cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [alertView setIconType:OpinionzAlertIconWarning];
        [alertView show];
    }
}


-(void) logout {
    
    [backendless.userService logout];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"loginView"];
    
    [self presentViewController: viewController animated:YES completion:nil];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"loggedIn"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
}

- (IBAction)keyboardNext:(id)sender {
    
    if(sender == self.nameTextfield){
        [self.nameTextfield resignFirstResponder];
        [self.subjectTextfield becomeFirstResponder];
    }
    else if(sender == self.subjectTextfield){
        [self.subjectTextfield resignFirstResponder];
        [self.messageTextView becomeFirstResponder];
    }
}

-(void)textViewDidBeginEditing:(UITextView *)textView{
    
    //[self.messageTextView setText:@""];

}

-(void)textViewDidEndEditing:(UITextView *)textView{
    
    if([self.messageTextView.text isEqualToString:@""]){
        [self.messageTextView setText:@"I am seeking counseling for: "];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

-(NSString *) getFullName {
    
    BackendlessUser *user = backendless.userService.currentUser;
    NSString *string = [NSString stringWithFormat:@"%@ %@", [user getProperty:@"firstname"], [user getProperty:@"lastname"]];
    NSLog(@"String: %@", string);
    return string;

}

-(void) openMenu{
    
    AppDelegate *appDelegate = [AppDelegate sharedInstanceofAppdelegate];
    [appDelegate.sideMenuViewController presentLeftMenuViewController];
}

-(void) openRightMenu {
    
    AppDelegate *appDelegate = [AppDelegate sharedInstanceofAppdelegate];
    [appDelegate.sideMenuViewController presentRightMenuViewController];
    
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"HighlightChapter" object:[NSNumber numberWithInt:(int)[appDelegate.book.chapters count] - 1] userInfo:nil];
}

-(void) goToChapter: (NSNotification *) notification{
    
    NSArray *array = [[notification object] componentsSeparatedByString:@","];
    NSLog(@"Array: %@", array);
    __block int counter = [array[0] intValue];
    
    AppDelegate *appDelegate = [AppDelegate sharedInstanceofAppdelegate];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"HomePageStoryboard" bundle:nil];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[storyboard instantiateViewControllerWithIdentifier:@"homePage"]];
    
    [appDelegate.sideMenuViewController setContentViewController:navigationController animated:YES];
    
    [appDelegate.sideMenuViewController hideMenuViewController];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"GoToChapter" object:[NSNumber numberWithInt:counter] userInfo:nil];
    });}




@end
