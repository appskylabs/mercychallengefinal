#import <Foundation/Foundation.h>

@interface Chapters : NSObject
@property (nonatomic, strong) NSString *objectId;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *keyPrinciple;
@property (nonatomic, strong) NSString *Explanation;
@property (nonatomic, strong) NSDate *created;
@property (nonatomic, strong) NSNumber *order;
@property (nonatomic, strong) NSString *ownerId;
@property (nonatomic, strong) NSDate *updated;
@property (nonatomic, strong) NSNumber *subheader;

@end