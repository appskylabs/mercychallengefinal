//
//  ContactUsViewController.h
//  1Mind->1Heart
//
//  Created by Taylor Korensky on 3/30/16.
//  Copyright © 2016 Appsky Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DonateViewController.h"
#import "UIHomePageViewController.h"
#import "OpinionzAlertView.h"
#import "Backendless.h"
#import "AppDelegate.h"

@interface ContactUsViewController : UIViewController <UITextViewDelegate>

@property BOOL isShowingMenu;


- (IBAction)facebookClicked:(id)sender;
- (IBAction)twitterClicked:(id)sender;
- (IBAction)websiteClicked:(id)sender;
- (IBAction)callToAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *backgroundView;
@property (strong, nonatomic) IBOutlet UIImageView *logoView;

@property (strong, nonatomic) IBOutlet UITextView *messageTextView;
@property (strong, nonatomic) IBOutlet UITextField *subjectTextfield;
@property (strong, nonatomic) IBOutlet UITextField *nameTextfield;
- (IBAction)keyboardNext:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *submitButton;
@end
