#import <Foundation/Foundation.h>

@interface Chapter : NSObject
@property (nonatomic, strong) NSDate *updated;
@property (nonatomic, strong) NSDate *created;
@property (nonatomic, strong) NSString *objectId;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *ownerId;
@property (nonatomic, strong) NSString *Explanation;
@property (nonatomic, strong) NSString *keyPrinciple;
@property  int order;
@end