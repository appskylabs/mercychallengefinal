//
//  LandingPageViewController.h
//  Mercy Challenge
//
//  Created by Taylor Korensky on 4/23/16.
//  Copyright © 2016 Appsky Labs. All rights reserved.
//

#import "AppDelegate.h"
#import <UIKit/UIKit.h>
#import "UIHomePageViewController.h"
#import "DonateViewController.h"
#import "ContactUsViewController.h"


@interface LandingPageViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *takeTheChallengeButton;
@property (strong, nonatomic) IBOutlet UIButton *memoryJournalButton;
@property (strong, nonatomic) IBOutlet UIButton *donateButton;
- (IBAction)takeTheChallengeClicked:(id)sender;
- (IBAction)memoryJournalClicked:(id)sender;
- (IBAction)donateClicked:(id)sender;

@end
