//
//  Challenge1RootViewController.m
//  Mercy Challenge
//
//  Created by Taylor Korensky on 4/23/16.
//  Copyright © 2016 Appsky Labs. All rights reserved.
//

#import "Challenge1RootViewController.h"

@interface Challenge1RootViewController ()

@end

@implementation Challenge1RootViewController

-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self setUpBackgroundImage];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIButton *aButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [aButton setBackgroundImage: [UIImage imageNamed: @"MenuSettings"] forState:UIControlStateNormal];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:aButton];
    [aButton addTarget:self action:@selector(openMenu) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = backButton;
    
    UIButton *rightButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [rightButton setBackgroundImage: [UIImage imageNamed: @"BookIcon"] forState:UIControlStateNormal];
    UIBarButtonItem *bookButton = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    [rightButton addTarget:self action:@selector(openRightMenu) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = bookButton;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goToChapter:) name:@"GoToChapterFromOtherView" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) openMenu{
    
    AppDelegate *appDelegate = [AppDelegate sharedInstanceofAppdelegate];
    [appDelegate.sideMenuViewController presentLeftMenuViewController]; 
}

-(void) openRightMenu {
    
    AppDelegate *appDelegate = [AppDelegate sharedInstanceofAppdelegate];
    [appDelegate.sideMenuViewController presentRightMenuViewController];
    
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"HighlightChapter" object:[NSNumber numberWithInt:(int)[appDelegate.book.chapters count] - 1] userInfo:nil];
}

-(void) setUpBackgroundImage{
    
    UIImageView *background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [background setImage:[UIImage imageNamed:@"sail7"]];
    [background setContentMode:UIViewContentModeScaleAspectFill];
    [background setAlpha:0.75];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [view setBackgroundColor:[UIColor blackColor]];
    [view setAlpha: 0.3];
    [self.view addSubview:view];
    [self.view sendSubviewToBack:view];
    [self.view addSubview:background];
    [self.view sendSubviewToBack:background];
}



-(void) goToChapter: (NSNotification *) notification{
    
    NSArray *array = [[notification object] componentsSeparatedByString:@","];
    NSLog(@"Array: %@", array);
    __block int counter = [array[0] intValue];
    
    AppDelegate *appDelegate = [AppDelegate sharedInstanceofAppdelegate];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"HomePageStoryboard" bundle:nil];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[storyboard instantiateViewControllerWithIdentifier:@"homePage"]];
    
    [appDelegate.sideMenuViewController setContentViewController:navigationController animated:YES];
    
    [appDelegate.sideMenuViewController hideMenuViewController];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"GoToChapter" object:[NSNumber numberWithInt:counter] userInfo:nil];
    });
}


- (IBAction)swipeRight:(id)sender {
    
    AppDelegate *appDelegate = [AppDelegate sharedInstanceofAppdelegate];

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"HomePageStoryboard" bundle:nil];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[storyboard instantiateViewControllerWithIdentifier:@"homePage"]];
    
    [appDelegate.sideMenuViewController setContentViewController:navigationController animated:YES];


}

@end
