//
//  AppDelegate.m
//  1Mind->1Heart
//
//  Created by Taylor Korensky on 3/26/16.
//  Copyright © 2016 Appsky Labs. All rights reserved.
//

#import "AppDelegate.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "Backendless.h"
#import "ViewController.h"
#import "UIHomePageViewController.h"

#define APP_ID @"734F4881-5E0F-C70A-FF1F-1C5BC7F50B00"
#define API_KEY @"4A0AB2C8-F57F-572B-FF7E-E1DB93682400"

#define SERVER_URL @"https://api.backendless.com"
#define backendless [Backendless sharedInstance]

@interface AppDelegate ()



@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.

    
    //self.book.chapters = [[[self.book.chapters reverseObjectEnumerator] allObjects] mutableCopy];

    //[self loadAppRootController];
   // [Fabric with:@[[Crashlytics class]]];
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
   
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    center.delegate = self;
    [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound |
                                             UNAuthorizationOptionAlert |
                                             UNAuthorizationOptionBadge)
                          completionHandler:^(BOOL granted, NSError * _Nullable error){
                              if(!error){
                                  
                                  [[UIApplication sharedApplication] performSelectorOnMainThread:@selector(registerForRemoteNotifications) withObject:nil waitUntilDone:YES modes:nil];
                                  
                                  //[[UIApplication sharedApplication]  registerForRemoteNotifications];
                              }
                          }];
    
    
    [backendless setHostUrl:SERVER_URL];
    [backendless initApp:APP_ID APIKey:API_KEY];
    // [backendless initApp:APP_ID secret:SECRET_KEY version:VERSION_NUM];
    // [backendless.messaging registerForRemoteNotifications];
    id<IDataStore> dataStore = [backendless.data of:[Book class]];
    self.book = [[Book alloc] init];
    self.book = (Book *)[dataStore findFirst];
    
    //NSLog(@"Book: %d", [self.book.chapters count]);
    //New way to get chapters
    LoadRelationsQueryBuilder *loadRelationsQueryBuilder =
    [LoadRelationsQueryBuilder of:[Chapters class]];
    [loadRelationsQueryBuilder setRelationName:@"chapters"];
    [loadRelationsQueryBuilder setPageSize:30];
    
    NSString *objectId = self.book.objectId;
    
    NSArray *chapters = [dataStore loadRelations:objectId queryBuilder:loadRelationsQueryBuilder];
    /* NSLog(@"Chapters Count: %d", [chapters count]);
     for (Chapters *chapter in chapters) {
     NSLog(@"%@", chapter.title);
     }*/
    self.book.chapters = [chapters mutableCopy];
    
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"order"
                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    self.book.chapters = [[self.book.chapters sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    UIStoryboard *storyboard1 = [UIStoryboard storyboardWithName:@"LandingPageStoryboard" bundle:nil];
    UIStoryboard *storyboard2 = [UIStoryboard storyboardWithName:@"LeftMenuStoryboard" bundle:nil];
    UIStoryboard *storyboard3 = [UIStoryboard storyboardWithName:@"RightMenuStoryboard" bundle:nil];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[storyboard1 instantiateViewControllerWithIdentifier:@"landingPage"]];
    LeftMenuViewController *leftMenuViewController = [storyboard2 instantiateViewControllerWithIdentifier:@"leftMenu"];
    
    RightMenuViewController *rightMenuViewController = [storyboard3 instantiateViewControllerWithIdentifier:@"rightMenu"];
    
    self.sideMenuViewController = [[RESideMenu alloc] initWithContentViewController:navigationController
                                                             leftMenuViewController:leftMenuViewController
                                                            rightMenuViewController: rightMenuViewController];
    
    self.sideMenuViewController.backgroundImage = [UIImage imageNamed:@"sailboats"];
    self.sideMenuViewController.menuPreferredStatusBarStyle = 1; // UIStatusBarStyleLightContent
    self.sideMenuViewController.delegate = self;
    self.sideMenuViewController.contentViewShadowColor = [UIColor blackColor];
    self.sideMenuViewController.contentViewShadowOffset = CGSizeMake(0, 0);
    self.sideMenuViewController.contentViewShadowOpacity = 0.6;
    self.sideMenuViewController.contentViewShadowRadius = 12;
    self.sideMenuViewController.contentViewShadowEnabled = YES;
    [self.sideMenuViewController setPanGestureEnabled:NO];
    self.window.rootViewController = self.sideMenuViewController;
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];

    return YES;
}

+ (AppDelegate*)sharedInstanceofAppdelegate
{
    return (AppDelegate *)[UIApplication sharedApplication].delegate;
}

- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken {
    // REGISTER DEVICE WITH BACKENDLESS HERE
    [backendless.messaging registerDevice:deviceToken];
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    // handle error
}


- (void)loadLoginViewController {
    
    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"loginView"]; // <storyboard id>
    
    self.window.rootViewController = viewController;
    [self.window makeKeyAndVisible];

}

-(void)loadAppRootController {
    
    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"HomePageStoryboard" bundle:nil];
    
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"homePage"]; // <storyboard id>
    
    self.window.rootViewController = viewController;
    [self.window makeKeyAndVisible];
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark -
#pragma mark RESideMenu Delegate

- (void)sideMenu:(RESideMenu *)sideMenu willShowMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"willShowMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(RESideMenu *)sideMenu didShowMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"didShowMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(RESideMenu *)sideMenu willHideMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"willHideMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(RESideMenu *)sideMenu didHideMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"didHideMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

@end
