#import <Foundation/Foundation.h>

@interface Memory : NSObject
@property (nonatomic, strong) NSNumber *rank2;
@property (nonatomic, strong) NSDate *updated;
@property (nonatomic, strong) NSString *event;
@property (nonatomic, strong) NSNumber *currentRanking;
@property (nonatomic, strong) NSString *objectId;
@property (nonatomic, strong) NSString *person;
@property (nonatomic, strong) NSNumber *rank1;
@property (nonatomic, strong) NSNumber *rank3;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSNumber *forgiven;
@property (nonatomic, strong) NSDate *created;
@property (nonatomic, strong) NSNumber *memoryWork;
@property (nonatomic, strong) NSString *ownerId;
@end