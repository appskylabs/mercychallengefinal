//
//  DonateViewController.m
//  1Mind->1Heart
//
//  Created by Taylor Korensky on 3/30/16.
//  Copyright © 2016 Appsky Labs. All rights reserved.
//

#import "DonateViewController.h"

@interface DonateViewController ()

@end

@implementation DonateViewController

-(void) viewWillAppear:(BOOL)animated{
    
    self.isShowingMenu = NO;
    //[self setUpMenu];
    
  /*  dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD show];
    });
    
    [self.webView setBackgroundColor:[UIColor blackColor]];
    self.webView.delegate = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://holyfamilycounseling.org/appdonate"]]];
        
    });*/
    

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view
    UIButton *aButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [aButton setBackgroundImage: [UIImage imageNamed: @"MenuSettings"] forState:UIControlStateNormal];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:aButton];
    [aButton addTarget:self action:@selector(openMenu) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = backButton;
    self.navigationItem.title = @"Donate";

    UIButton *rightButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [rightButton setBackgroundImage: [UIImage imageNamed: @"BookIcon"] forState:UIControlStateNormal];
    UIBarButtonItem *bookButton = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    [rightButton addTarget:self action:@selector(openRightMenu) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = bookButton;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goToChapter:) name:@"GoToChapterFromOtherView" object:nil];

}

-(void) viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:animated];
    
  //  [SVProgressHUD dismiss];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*-(void) setUpMenu {
    
    
    NSMutableArray *menuItems = [[NSMutableArray alloc] init];
    REMenuItem *homeItem = [[REMenuItem alloc] initWithTitle:@"Mercy Challenge"
                                                       image:nil
                                            highlightedImage:nil
                                                      action:^(REMenuItem *item) {
                                                          
                                                          [self.menu close];
                                                          self.isShowingMenu = NO;
                                                          UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"HomePageStoryboard" bundle:nil];
                                                          UIHomePageViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"homePage"];
                                                          
                                                          [self presentViewController: viewController animated:YES completion:nil];
                                                      }];
    [menuItems addObject:homeItem];
    
    REMenuItem *contactUsItem = [[REMenuItem alloc] initWithTitle:@"Contact Us"
                                                         image:nil
                                              highlightedImage:nil
                                                        action:^(REMenuItem *item) {
                                                            
                                                            [self.menu close];
                                                            self.isShowingMenu = NO;
                                                            
                                                            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ContactUsStoryboard" bundle:nil];
                                                            ContactUsViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"contactUsView"];
                                                            [self presentViewController: viewController animated:YES completion:nil];
                                                            
                                                            
                                                        }];
    [menuItems addObject:contactUsItem];
    
    
    REMenuItem *logoutItem = [[REMenuItem alloc] initWithTitle:@"Logout"
                                                         image:nil
                                              highlightedImage:nil
                                                        action:^(REMenuItem *item) {
                                                            
                                                            
                                                            [self.menu close];
                                                            self.isShowingMenu = NO;
                                                            [self logout];
                                                        }];
    [menuItems addObject:logoutItem];
    
    
    
    self.menu = [[REMenu alloc] initWithItems:menuItems];
    
    [self.menu setTextColor:[UIColor colorWithRed:0.9568 green:0.7788 blue:0.1842 alpha:1.0]];
    [self.menu setBackgroundAlpha:0.7f];
}

- (IBAction)menuClicked:(id)sender {
    
    if(self.isShowingMenu != YES){
        
        [self.menu showFromRect:CGRectMake(0, 72, 420, 300) inView:self.view];
        self.isShowingMenu = YES;
    }
    else{
        
        
        [self.menu close];
        self.isShowingMenu = NO;
    }
    
}
*/

-(void) logout {
    
    [backendless.userService logout];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"loginView"];
    
    [self presentViewController: viewController animated:YES completion:nil];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"loggedIn"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
}


-(void)webViewDidFinishLoad:(UIWebView *)webView{
    
    //[SVProgressHUD dismiss];
}

-(void) openMenu{
    
    AppDelegate *appDelegate = [AppDelegate sharedInstanceofAppdelegate];
    [appDelegate.sideMenuViewController presentLeftMenuViewController];
}

-(void) openRightMenu {
    
    AppDelegate *appDelegate = [AppDelegate sharedInstanceofAppdelegate];
    [appDelegate.sideMenuViewController presentRightMenuViewController];
    
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"HighlightChapter" object:[NSNumber numberWithInt:(int)[appDelegate.book.chapters count] - 1] userInfo:nil];
}
-(void) goToChapter: (NSNotification *) notification{
    
    NSArray *array = [[notification object] componentsSeparatedByString:@","];
    NSLog(@"Array: %@", array);
    __block int counter = [array[0] intValue];
    
    AppDelegate *appDelegate = [AppDelegate sharedInstanceofAppdelegate];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"HomePageStoryboard" bundle:nil];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[storyboard instantiateViewControllerWithIdentifier:@"homePage"]];
    
    [appDelegate.sideMenuViewController setContentViewController:navigationController animated:YES];
    
    [appDelegate.sideMenuViewController hideMenuViewController];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"GoToChapter" object:[NSNumber numberWithInt:counter] userInfo:nil];
    });
}



- (IBAction)goToDonatePage:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://holyfamilycounseling.org/appdonate"]];

    
}
@end
