//
//  Challenge2ShareViewController.h
//  Mercy Challenge
//
//  Created by Taylor Korensky on 5/2/16.
//  Copyright © 2016 Appsky Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>


@interface Challenge2ShareViewController : UIViewController <MFMailComposeViewControllerDelegate>

- (IBAction)shareWithFacebook:(id)sender;
- (IBAction)shareWithTwitter:(id)sender;
- (IBAction)shareWithEmail:(id)sender;


@end
