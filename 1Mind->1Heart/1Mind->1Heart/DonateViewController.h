//
//  DonateViewController.h
//  1Mind->1Heart
//
//  Created by Taylor Korensky on 3/30/16.
//  Copyright © 2016 Appsky Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContactUsViewController.h"
#import "UIHomePageViewController.h"
#import "OpinionzAlertView.h"
#import "Backendless.h"
#import "AppDelegate.h"

@interface DonateViewController : UIViewController <UIWebViewDelegate>

- (IBAction)goToDonatePage:(id)sender;
@property BOOL isShowingMenu;

@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@end
