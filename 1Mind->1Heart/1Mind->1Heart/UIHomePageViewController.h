//
//  UIHomePageViewController.h
//  1Mind->1Heart
//
//  Created by Taylor Korensky on 3/28/16.
//  Copyright © 2016 Appsky Labs. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import "ContactUsViewController.h"
#import "DonateViewController.h"
#import "AppDelegate.h"
#import "Book.h"
#import "Chapter.h"
#import "Challenge1RootViewController.h"


@interface UIHomePageViewController : UIViewController 
@property (strong, nonatomic) Book *book;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property int counter;
@property (strong, nonatomic) NSMutableArray *chaptersArray;
@property (strong, nonatomic) IBOutlet UILabel *keyPrincipleLabel;
@property (strong, nonatomic) IBOutlet UITextView *keyPrincipleTextView;
@property (strong, nonatomic) IBOutlet UITextView *explanationTextView;
@property (strong, nonatomic) IBOutlet UILabel *explanationLabel;

- (IBAction)leftButtonClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *leftButton;
@property (strong, nonatomic) IBOutlet UIButton *rightButton;
- (IBAction)rightButtonClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@end
