#import <Foundation/Foundation.h>

@interface MercyTracker : NSObject
@property (nonatomic, strong) NSString *objectId;
@property (nonatomic, strong) NSDate *created;
@property (nonatomic, strong) NSString *diocese;
@property (nonatomic, strong) NSString *hurtsHealed;
@property (nonatomic, strong) NSString *ownerId;
@property (nonatomic, strong) NSString *hearAbout;
@property (nonatomic, strong) NSString *howManyForgiven;
@property (nonatomic, strong) NSString *feedback;
@property (nonatomic, strong) NSString *impactOnLife;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *howManyChallenged;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSDate *updated;
@end