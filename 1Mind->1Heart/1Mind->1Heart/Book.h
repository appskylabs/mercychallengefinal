#import <Foundation/Foundation.h>

@class Chapters;

@interface Book : NSObject
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *ownerId;
@property (nonatomic, strong) NSDate *updated;
@property (nonatomic, strong) NSString *objectId;
@property (nonatomic, strong) NSDate *created;
@property (nonatomic, strong) NSString *author;
@property (nonatomic, strong) NSMutableArray *chapters;

-(void)addToChapters:(Chapters *)chapter;
-(void)removeFromChapters:(Chapters *)chapter;
-(NSMutableArray *)loadChapters;
-(void)freeChapters;
@end