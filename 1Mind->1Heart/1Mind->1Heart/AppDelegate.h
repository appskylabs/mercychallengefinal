//
//  AppDelegate.h
//  1Mind->1Heart
//
//  Created by Taylor Korensky on 3/26/16.
//  Copyright © 2016 Appsky Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Book.h"
#import "Chapters.h"
#include "RESideMenu.h"
#import "LeftMenuViewController.h"
#import "UIHomePageViewController.h"
#import "RightMenuViewController.h"
#import <UserNotifications/UserNotifications.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, RESideMenuDelegate, UNUserNotificationCenterDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) Book *book;
+ (AppDelegate*)sharedInstanceofAppdelegate;

@property (strong, nonatomic) RESideMenu *sideMenuViewController;
@end

