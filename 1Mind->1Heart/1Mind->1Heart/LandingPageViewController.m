//
//  LandingPageViewController.m
//  Mercy Challenge
//
//  Created by Taylor Korensky on 4/23/16.
//  Copyright © 2016 Appsky Labs. All rights reserved.
//

#import "LandingPageViewController.h"

@interface LandingPageViewController ()

@end

@implementation LandingPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"clickedChallenge"]){
        
          [self.takeTheChallengeButton setTitle:@"Continue Reading" forState:UIControlStateNormal];
    }
    else{
        
        [self.takeTheChallengeButton setTitle:@"Begin Reading" forState:UIControlStateNormal];
    }
    
    
    // Do any additional setup after loading the view.
    self.takeTheChallengeButton.layer.borderWidth = 1.5f;
    self.takeTheChallengeButton.layer.borderColor = [UIColor whiteColor].CGColor;
    
    self.memoryJournalButton.layer.borderWidth = 1.5f;
    self.memoryJournalButton.layer.borderColor = [UIColor whiteColor].CGColor;
    
    self.donateButton.layer.borderWidth = 1.5f;
    self.donateButton.layer.borderColor = [UIColor whiteColor].CGColor;
    
    UIButton *aButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [aButton setBackgroundImage: [UIImage imageNamed: @"MenuSettings"] forState:UIControlStateNormal];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:aButton];
    [aButton addTarget:self action:@selector(openMenu) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = backButton;

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)takeTheChallengeClicked:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"clickedChallenge"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    AppDelegate *appDelegate = [AppDelegate sharedInstanceofAppdelegate];

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"HomePageStoryboard" bundle:nil];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[storyboard instantiateViewControllerWithIdentifier:@"homePage"]];
    [appDelegate.sideMenuViewController setContentViewController:navigationController animated:YES];

}

- (IBAction)memoryJournalClicked:(id)sender {
    
    AppDelegate *appDelegate = [AppDelegate sharedInstanceofAppdelegate];

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Challenge1Storyboard" bundle:nil];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[storyboard instantiateViewControllerWithIdentifier:@"memoryListView"]];
    [appDelegate.sideMenuViewController setContentViewController:navigationController];

}

- (IBAction)donateClicked:(id)sender {
    
    AppDelegate *appDelegate = [AppDelegate sharedInstanceofAppdelegate];

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"DonateStoryboard" bundle:nil];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[storyboard instantiateViewControllerWithIdentifier:@"donateView"]];
    [appDelegate.sideMenuViewController setContentViewController:navigationController animated:YES];
}

-(void) openMenu{
    
    AppDelegate *appDelegate = [AppDelegate sharedInstanceofAppdelegate];
    [appDelegate.sideMenuViewController presentLeftMenuViewController];
}


@end
