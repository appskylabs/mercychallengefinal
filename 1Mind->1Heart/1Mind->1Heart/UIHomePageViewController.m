//
//  UIHomePageViewController.m
//  1Mind->1Heart
//
//  Created by Taylor Korensky on 3/28/16.
//  Copyright © 2016 Appsky Labs. All rights reserved.
//

#import "UIHomePageViewController.h"
#import "OpinionzAlertView.h"
#import "Backendless.h"


@interface UIHomePageViewController ()

@end

@implementation UIHomePageViewController

-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goToChapter:) name:@"GoToChapter" object:nil];
    
    AppDelegate *appDelegate = [AppDelegate sharedInstanceofAppdelegate];
    self.chaptersArray = [[NSMutableArray alloc] init];
    self.book = [[Book alloc] init];
    self.book = appDelegate.book;
    self.chaptersArray  = self.book.chapters;
    NSLog(@"Chapters Array Count: %d", (int)self.chaptersArray.count);
    
    self.counter = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"currentPage"];
    if(self.counter == 0){
        [self.leftButton setHidden:YES];
    }
    if(self.chaptersArray.count == 1)
    {
        [self.rightButton setHidden:YES];
    }
    
    [self.explanationTextView setScrollsToTop:YES];
    [self.keyPrincipleTextView setScrollsToTop:YES];
    
    [self.keyPrincipleLabel setHidden:YES];
    [self.explanationLabel setHidden:YES];
    
    [self performSelector:@selector(nextPage) withObject:nil afterDelay:0.5];
    
    [self setUpBackgroundImage];
    
    if(self.view.frame.size.height <= 568){
        
        [self.explanationTextView setFont:[UIFont fontWithName:@"Verdana" size:12]];
        [self.keyPrincipleTextView setFont:[UIFont fontWithName:@"Verdana" size:12]];
    }
}

-(void) setUpBackgroundImage{
    
    UIImageView *background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [background setImage:[UIImage imageNamed:@"sail3"]];
    [background setContentMode:UIViewContentModeScaleAspectFill];
    [background setAlpha:0.75];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [view setBackgroundColor:[UIColor blackColor]];
    [view setAlpha: 0.3];
    [self.view addSubview:view];
    [self.view sendSubviewToBack:view];
    [self.view addSubview:background];
    [self.view sendSubviewToBack:background];
}

-(void ) viewDidLoad{
    
    [super viewDidLoad];
    

    
   UIButton *aButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [aButton setBackgroundImage: [UIImage imageNamed: @"MenuSettings"] forState:UIControlStateNormal];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:aButton];
    [aButton addTarget:self action:@selector(openMenu) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = backButton;
    
    UIButton *rightButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [rightButton setBackgroundImage: [UIImage imageNamed: @"BookIcon"] forState:UIControlStateNormal];
    UIBarButtonItem *bookButton = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    [rightButton addTarget:self action:@selector(openRightMenu) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = bookButton;
    
    self.navigationItem.title = @"Healing Memories";

}

-(void) viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    [self.scrollView setContentSize:CGSizeMake(self.view.frame.size.width - 40, 1300)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    //need array count here for dynamic stuff
    if(self.counter > (self.chaptersArray.count - 1)){
        self.counter = ((int)self.chaptersArray.count - 1);
    }
    [[NSUserDefaults standardUserDefaults] setInteger:self.counter forKey:@"currentPage"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


-(void)webViewDidFinishLoad:(UIWebView *)webView{
    
        [SVProgressHUD dismiss];
}


-(void) logout {

    [backendless.userService logout];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"loginView"];

    [self presentViewController: viewController animated:YES completion:nil];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"loggedIn"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void) openMenu{
    
    AppDelegate *appDelegate = [AppDelegate sharedInstanceofAppdelegate];
    [appDelegate.sideMenuViewController presentLeftMenuViewController];
}

-(void) openRightMenu {
    
    AppDelegate *appDelegate = [AppDelegate sharedInstanceofAppdelegate];
    [appDelegate.sideMenuViewController presentRightMenuViewController];
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"HighlightChapter" object:[NSNumber numberWithInt:self.counter] userInfo:nil];
}

-(void) nextPage {
   
    if(self.counter == self.chaptersArray.count){
        
        AppDelegate *appDelegate = [AppDelegate sharedInstanceofAppdelegate];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Challenge1Storyboard" bundle:nil];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[storyboard instantiateViewControllerWithIdentifier:@"challenge1Root"]];
        [appDelegate.sideMenuViewController setContentViewController:navigationController];

    }else{
   
        [self.titleLabel setText:[(Chapter *)[self.chaptersArray objectAtIndex:self.counter] title]];
   
        [self.keyPrincipleTextView setText:[(Chapter *)[self.chaptersArray objectAtIndex:self.counter] keyPrinciple]];
   
        [self.explanationTextView setText:[(Chapter *)[self.chaptersArray objectAtIndex:self.counter] Explanation]];
        

    if([[(Chapter *)[self.chaptersArray objectAtIndex:self.counter] Explanation] length] == 0 )
    {
        [self.explanationLabel setHidden:YES];
    }
    else{
        
        [self.explanationLabel setHidden:NO];
    }
        
        if([[[self.chaptersArray objectAtIndex:self.counter] subheader] intValue] != 0 || [[(Chapter *)[self.chaptersArray objectAtIndex:self.counter] title] isEqualToString:@"Preface"]){
            
            [self.keyPrincipleLabel setHidden:YES];
            [self.explanationLabel setHidden:YES];
        }
        else{
            
            [self.keyPrincipleLabel setHidden:NO];
            [self.explanationLabel setHidden:NO];
        }
        
    [self.scrollView setContentOffset:
         CGPointMake(0, -self.scrollView.contentInset.top) animated:YES];
    [self.explanationTextView setContentOffset:CGPointZero animated:YES];
    [self.keyPrincipleTextView setContentOffset:CGPointZero animated:YES];
    
        if(self.counter == 0){
            [self.leftButton setHidden:YES];
        }
    }
}

- (IBAction)leftButtonClicked:(id)sender {
    
    if(self.counter > 0){
        self.counter--;
        [self.rightButton setHidden:NO];
    }
    else{
        [self.leftButton setHidden:YES];
        [self.rightButton setHidden:NO];
    }
  
    
    [self nextPage];
    
}
- (IBAction)rightButtonClicked:(id)sender {
    
    if(self.counter < self.chaptersArray.count){
        self.counter++;
    }
    if(self.counter == self.chaptersArray.count){
        [self.rightButton setHidden:YES];
    }
    if(self.counter > 0){
        [self.leftButton setHidden:NO];
    }
    [self nextPage];
}

-(void) goToChapter: (NSNotification *) notification{
    
    NSLog(@"Go To Chapter");
    self.counter = [[notification object] intValue];
    if(self.counter == 0){
        [self.leftButton setHidden:YES];
    }
    else{
        [self.leftButton setHidden:NO];
    }
    if(self.chaptersArray.count == 1)
    {
        [self.rightButton setHidden:YES];
    }
    else{
        [self.rightButton setHidden:NO];
    }

    [self nextPage];
}

@end
