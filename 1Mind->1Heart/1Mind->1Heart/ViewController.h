//
//  ViewController.h
//  1Mind->1Heart
//
//  Created by Taylor Korensky on 3/26/16.
//  Copyright © 2016 Appsky Labs. All rights reserved.
//



#import <UIKit/UIKit.h>
#import "UIImageView+PlayGIF.h"
#import "JJMaterialTextField.h"
#import "OpinionzAlertView.h"
#import "Backendless.h"
#import "UIHomePageViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>


typedef enum {
    LOGIN,
    SIGNUP
} LoginOrSignUp;

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *backgroundGIF;
@property (strong, nonatomic) IBOutlet JJMaterialTextfield *emailTextField;
@property (strong, nonatomic) IBOutlet JJMaterialTextfield *passwordTextField;
- (IBAction)keyboardNext:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *loginSignUpButton;

@property (strong, nonatomic) IBOutlet UIImageView *iconImageView;
@property (strong, nonatomic) IBOutlet JJMaterialTextfield *emailSignUpTextField;
@property (strong, nonatomic) IBOutlet JJMaterialTextfield *firstNameTextField;
@property (strong, nonatomic) IBOutlet JJMaterialTextfield *lastNameTextField;
@property (strong, nonatomic) IBOutlet JJMaterialTextfield *passwordSignUpTextField;
@property (strong, nonatomic) IBOutlet JJMaterialTextfield *passwordVerifySignUpTextField;
@property (strong, nonatomic) IBOutlet UIButton *signUpButton;
- (IBAction)signUpClicked:(id)sender;
- (IBAction)loginSignUpButtonClicked:(id)sender;

@property LoginOrSignUp loginOrSignUp;
@property (strong, nonatomic) IBOutlet UIButton *resetPasswordButton;

- (IBAction)resetPassword:(id)sender;

- (IBAction)dismissKeyboard:(id)sender;
@end

