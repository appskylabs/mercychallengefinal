//
//  MemoryViewController.h
//  Mercy Challenge
//
//  Created by Taylor Korensky on 4/24/16.
//  Copyright © 2016 Appsky Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OpinionzAlertView.h"
#import "Backendless.h"
#import "Memory.h"


typedef enum {
    CREATE,
    EDIT
} MemoryType;


@interface MemoryViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *personTextField;
@property (strong, nonatomic) IBOutlet UITextField *eventTextField;
@property (strong, nonatomic) IBOutlet UISegmentedControl *forgivenSegmentViewController;

@property (strong, nonatomic) IBOutlet UISegmentedControl *memoryWorkSegmentController;
@property (strong, nonatomic) IBOutlet UILabel *rank1Label;
@property (strong, nonatomic) IBOutlet UIStepper *rank1Stepper;
@property (strong, nonatomic) IBOutlet UILabel *rank2Label;
@property (strong, nonatomic) IBOutlet UIStepper *rank2Stepper;
@property (strong, nonatomic) IBOutlet UILabel *rank3Label;
@property (strong, nonatomic) IBOutlet UIStepper *rank3Stepper;
@property (strong, nonatomic) IBOutlet UIButton *saveMemoryButton;
- (IBAction)saveMemoryClicked:(id)sender;
@property BOOL rankUp;
- (IBAction)valueChanged:(id)sender;

- (IBAction)keyboardNext:(id)sender;

-(void) setUpView: (Memory *)memory;
@property (strong, nonatomic) Memory *memory;

@property MemoryType typeId;

@end
