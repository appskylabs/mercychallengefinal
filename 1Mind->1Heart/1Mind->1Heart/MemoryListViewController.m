//
//  MemoryListViewController.m
//  Mercy Challenge
//
//  Created by Taylor Korensky on 4/25/16.
//  Copyright © 2016 Appsky Labs. All rights reserved.
//

#import "MemoryListViewController.h"

@interface MemoryListViewController ()

@end

@implementation MemoryListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.memoryArray = [[NSMutableArray alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;

    UIButton *rightButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [rightButton setBackgroundImage: [UIImage imageNamed: @"BookIcon"] forState:UIControlStateNormal];
    UIBarButtonItem *bookButton = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    [rightButton addTarget:self action:@selector(openRightMenu) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = bookButton;
   
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goToChapter:) name:@"GoToChapterFromOtherView" object:nil];


}

-(void) viewWillAppear:(BOOL)animated{
    


}

-(void) viewDidAppear:(BOOL)animated{
    
    [self checkRegisterStuff];
    
    
}

-(void) viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:animated];
    
    [SVProgressHUD dismiss];
}

-(void) checkRegisterStuff {
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"createdAccount"]){ //have an account already
        
        if([backendless.userService currentUser] == nil){
            
            [self loginUser];
        }
        else{ //normal launch
            
            [self getMemories];
            
        }
    }
    else{  //dont have an account yet
        
        [self registerUser];
        
    }

}

/***********NEEED TO UPDATE THIS********************/
-(void) getMemories{
    
    //commented out because its causing errors
    
   [SVProgressHUD show];
    [self.memoryArray removeAllObjects];
    //BackendlessDataQuery *query = [BackendlessDataQuery query];
    
    NSString *string = [[backendless.userService currentUser] getObjectId];
   // query.whereClause = [NSString stringWithFormat:@"userId = \'%@\'", string];
    NSString *whereClause = [NSString stringWithFormat:@"userId = \'%@\'", string];
    DataQueryBuilder *queryBuilder = [[DataQueryBuilder alloc] init];
    [queryBuilder setWhereClause:whereClause];
    
    id<IDataStore>dataStore = [backendless.data of:[Memory class]];
    [dataStore find:queryBuilder
           response:^(NSArray<Memory *> *memoryList) {
               NSLog(@"Result: %d", (int)memoryList.count);
               self.memoryArray = (NSMutableArray *)[memoryList mutableCopy];
               [self.tableView reloadData];
               [SVProgressHUD dismiss];
               
           }
              error:^(Fault *fault) {
                  NSLog(@"Server reported an error: %@", fault);
                  [SVProgressHUD dismiss];
              }
     ];
  /*  [[backendless.persistenceService of:[Memory class]] find:query response:^(BackendlessCollection *collection) {
        
        self.memoryArray = (NSMutableArray *)[collection data];
        NSLog(@"Collection: %@", self.memoryArray);
        [self.tableView reloadData];
        [SVProgressHUD dismiss];
        
    } error:^(Fault *fault) {
        NSLog(@"Fault: %@", fault.message);
        [SVProgressHUD dismiss];
    }];*/

}


-(void) loginUser{
    
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Login"
                                          message:@"Please login to your Mercy Challenge account."
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"email";
     }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"password";
     }];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   NSString *email = ((UITextField *)alertController.textFields.firstObject).text;
                                   
                                   NSString *password = ((UITextField *)alertController.textFields[1]).text;
                                   
                                   NSLog(@"Email address entered: %@", email);
                                   
                                   if(email.length > 0 && password.length > 0){
                                       
                                       if(password.length > 5){
                                           
                                           [backendless.userService login:email password:password response:^(BackendlessUser *user) {
                                               [backendless.userService setStayLoggedIn:YES];
                                               [alertController dismissViewControllerAnimated:YES completion:nil];
                                               [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"createdAccount"];
                                               [[NSUserDefaults standardUserDefaults] synchronize];
                                               [self getMemories];
                                               
                                           } error:^(Fault *fault) {
                                              
                                               OpinionzAlertView *alertView = [[OpinionzAlertView alloc] initWithTitle:@"Error" message: @"There was an error with the login." cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                                               [alertView setIconType:OpinionzAlertIconWarning];
                                               [alertView show];

                                               
                                                [self performSelector:@selector(checkRegisterStuff) withObject:nil afterDelay:1.00];
                                               
                                           }];

                                       }
                                       else{
                                            [alertController dismissViewControllerAnimated:YES completion:nil];
                                           OpinionzAlertView *alertView = [[OpinionzAlertView alloc] initWithTitle:@"Error" message: @"Make sure password is at least 6 characters." cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                                           [alertView setIconType:OpinionzAlertIconWarning];
                                           [alertView show];
                                           [self performSelector:@selector(checkRegisterStuff) withObject:nil afterDelay:1.00];

                                       }
                                       
                                   }
                                   else{
                                       
                                       [alertController dismissViewControllerAnimated:YES completion:nil];
                                       OpinionzAlertView *alertView = [[OpinionzAlertView alloc] initWithTitle:@"Error" message: @"Make sure all fields are filled out." cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                                       [alertView setIconType:OpinionzAlertIconWarning];
                                       [alertView show];
                                        [self performSelector:@selector(checkRegisterStuff) withObject:nil afterDelay:1.00];

                                   }
                               }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alertController dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    
    [alertController addAction:cancel];
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}


/************************NEEED TO UPDATE THIS*************************/
-(void) registerUser {
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Register"
                                          message:@"Please create a free one-time login account, or login, so you can access your challenge and journal on any of your devices."
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"email";
     }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"password";
     }];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"Register"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   NSString *email = ((UITextField *)alertController.textFields.firstObject).text;
                                   
                                   NSString *password = ((UITextField *)alertController.textFields[1]).text;
                                   
                                   NSLog(@"Email address entered: %@", email);
                                   
                                   if(email.length > 0 && password.length > 0){
                                       
                                       if(password.length > 5){
                                           
                                           BackendlessUser *user = [BackendlessUser new];
                                           user.password = password;
                                           user.email = email;
                                           
                                           
                                           [backendless.userService registerUser:user response:^(BackendlessUser *response) {
                                               
                                            
                                               [backendless.userService login:email password:password response:^(BackendlessUser *user) {
                                                   [backendless.userService setStayLoggedIn:YES];
                                                   [alertController dismissViewControllerAnimated:YES completion:nil];
                                                   [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"createdAccount"];
                                                   [[NSUserDefaults standardUserDefaults] synchronize];
                                                   
                                               } error:^(Fault *fault) {
                                                   
                                                   OpinionzAlertView *alertView = [[OpinionzAlertView alloc] initWithTitle:@"Error" message: @"Error with the login." cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                                                   [alertView setIconType:OpinionzAlertIconWarning];
                                                   [alertView show];
                                                   
                                                   
                                                   [self performSelector:@selector(checkRegisterStuff) withObject:nil afterDelay:1.00];
                                                   
                                               }];
                                               
                                           } error:^(Fault *fault) {
                                               
                                               OpinionzAlertView *alertView = [[OpinionzAlertView alloc] initWithTitle:@"Error" message: @"Error with the registration." cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                                               [alertView setIconType:OpinionzAlertIconWarning];
                                               [alertView show];
                                               
                                               
                                               [self performSelector:@selector(checkRegisterStuff) withObject:nil afterDelay:1.00];
                                           }];
                                           
                                       }
                                       else{
                                        
                                           [alertController dismissViewControllerAnimated:YES completion:nil];
                                           OpinionzAlertView *alertView = [[OpinionzAlertView alloc] initWithTitle:@"Error" message: @"Make sure password is at least 6 characters." cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                                           [alertView setIconType:OpinionzAlertIconWarning];
                                           [alertView show];
                                           [self performSelector:@selector(checkRegisterStuff) withObject:nil afterDelay:1.00];
                                       }
                                       
                                   }
                                   else{
                                       
                                       [alertController dismissViewControllerAnimated:YES completion:nil];
                                       OpinionzAlertView *alertView = [[OpinionzAlertView alloc] initWithTitle:@"Error" message: @"Make sure all fields are filled out." cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                                       [alertView setIconType:OpinionzAlertIconWarning];
                                       [alertView show];
                                       [self performSelector:@selector(checkRegisterStuff) withObject:nil afterDelay:1.00];
                                   }
                               }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alertController dismissViewControllerAnimated:YES completion:nil];
                                 [self.navigationController popViewControllerAnimated:YES];
                                 
                             }];
    UIAlertAction* login = [UIAlertAction
                             actionWithTitle:@"Have an account?"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                             
                             
                                 [alertController dismissViewControllerAnimated:YES completion:^{
                                     
                                     
                                 }];
                                    [self loginUser];
                                 
                             }];

    
    
    [alertController addAction:okAction];
     [alertController addAction:login];
    [alertController addAction:cancel];

   
    
    [self presentViewController:alertController animated:YES completion:nil];


}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark TableViewDelegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if( cell == nil ) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    Memory *tempMemory = [self.memoryArray objectAtIndex:indexPath.row];
  
    [cell setBackgroundColor:[UIColor clearColor]];
    
  /*  UILabel *personLabel = (UILabel *)[cell viewWithTag:1];
    [personLabel setText: tempMemory.person];*/
    
    
    UILabel *eventLabel = (UILabel *)[cell viewWithTag:2];
    [eventLabel setText: tempMemory.event];
    
    UILabel *forgiveLabel = (UILabel *)[cell viewWithTag:3];
    if([tempMemory.forgiven boolValue]){
        [forgiveLabel setText:@"YES"];

    }
    else{
        [forgiveLabel setText:@"NO"];
    }
    
    UILabel *memWorkLabel = (UILabel *)[cell viewWithTag:4];
    if([tempMemory.memoryWork boolValue]){
        [memWorkLabel setText:@"YES"];
    }
    else{
        [memWorkLabel setText:@"NO"];
    }
    
     UILabel *rank1Label = (UILabel *)[cell viewWithTag:5];
    [rank1Label setText:[NSString stringWithFormat:@"%d",[ tempMemory.rank1 intValue]]];
 
    UILabel *rank2Label = (UILabel *)[cell viewWithTag:6];
    [rank2Label setText:[NSString stringWithFormat:@"%d",[ tempMemory.rank2 intValue]]];
    
   /* UILabel *rank3Label = (UILabel *)[cell viewWithTag:7];
    [rank3Label setText:[NSString stringWithFormat:@"%d",[ tempMemory.rank3 intValue]]];*/
    
    
        return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.memoryArray.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Challenge1Storyboard" bundle:nil];
    MemoryViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"memoryView"];
    [viewController setUpView: [self.memoryArray objectAtIndex:indexPath.row]];
    [self.navigationController showViewController:viewController sender:self];
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        NSLog(@"Delete :%@", [self.memoryArray objectAtIndex:indexPath.row]);
        id<IDataStore> dataStore = [backendless.persistenceService of:[Memory class]];
        [dataStore remove:[self.memoryArray objectAtIndex:indexPath.row] response:^(NSNumber *number) {
            
            [self.memoryArray removeObjectAtIndex:indexPath.row];
            [self.tableView reloadData];
            
        } error:^(Fault *fault) {
            
            OpinionzAlertView *alertView = [[OpinionzAlertView alloc] initWithTitle:@"Error" message: @"There was an error deleting this memory at this time." cancelButtonTitle:@"Okay" otherButtonTitles:nil];
            [alertView setIconType:OpinionzAlertIconWarning];
            [alertView show];
            
        }];
        
    }
}


- (IBAction)addMemoryClicked:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Challenge1Storyboard" bundle:nil];
    MemoryViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"memoryView"];
    [viewController setUpView: nil];
    [self.navigationController showViewController:viewController sender:self];

}


-(void) openRightMenu {
    
    AppDelegate *appDelegate = [AppDelegate sharedInstanceofAppdelegate];
    [appDelegate.sideMenuViewController presentRightMenuViewController];
    
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"HighlightChapter" object:[NSNumber numberWithInt:(int)[appDelegate.book.chapters count] - 1] userInfo:nil];
}

-(void) goToChapter: (NSNotification *) notification{
    
    NSArray *array = [[notification object] componentsSeparatedByString:@","];
    NSLog(@"Array: %@", array);
    __block int counter = [array[0] intValue];
    
    AppDelegate *appDelegate = [AppDelegate sharedInstanceofAppdelegate];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"HomePageStoryboard" bundle:nil];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[storyboard instantiateViewControllerWithIdentifier:@"homePage"]];
    
    [appDelegate.sideMenuViewController setContentViewController:navigationController animated:YES];
    
    [appDelegate.sideMenuViewController hideMenuViewController];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"GoToChapter" object:[NSNumber numberWithInt:counter] userInfo:nil];
    });}


@end
