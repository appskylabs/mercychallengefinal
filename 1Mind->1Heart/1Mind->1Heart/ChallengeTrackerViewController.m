//
//  ChallengeTrackerViewController.m
//  Mercy Challenge
//
//  Created by Taylor Korensky on 5/2/16.
//  Copyright © 2016 Appsky Labs. All rights reserved.
//

#import "ChallengeTrackerViewController.h"

@interface ChallengeTrackerViewController ()

@end

@implementation ChallengeTrackerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUpBackgroundImage];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goToChapter:) name:@"GoToChapterFromOtherView" object:nil];

    
    UIButton *rightButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [rightButton setBackgroundImage: [UIImage imageNamed: @"BookIcon"] forState:UIControlStateNormal];
    UIBarButtonItem *bookButton = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    [rightButton addTarget:self action:@selector(openRightMenu) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = bookButton;

    if([[NSUserDefaults standardUserDefaults] boolForKey:@"createdAccount"]){ //have an account already
        
        if([backendless.userService currentUser] == nil){
            
            [self loginUser];
        }
        else{ //normal launch
            
            if([[NSUserDefaults standardUserDefaults] boolForKey:@"trackerDone"]){
                
                [self showSkipAlert];
            }
            
        }
    }
    else{  //dont have an account yet
        
        [self registerUser];
        
    }
    
    

}

-(void) showSkipAlert{
    
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Already Submitted"
                                          message:@"You have already submitted a Challenge Tracker. If you would like you can fill it out again or skip to the next step."
                                          preferredStyle:UIAlertControllerStyleAlert];
    

    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"Submit Again"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [alertController dismissViewControllerAnimated:YES completion:nil];
                               
                               }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Skip"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [self goToTestimonialView];
                             }];
    
    
    [alertController addAction:cancel];
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}

-(void) goToTestimonialView {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Challenge2Storyboard" bundle:nil];
    TestimonialViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"testimonialView"];
    [self.navigationController showViewController:viewController sender:self];
}


-(void) loginUser{
    
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Login"
                                          message:@"Please login to your Mercy Challenge account."
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"email";
     }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"password";
     }];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   NSString *email = ((UITextField *)alertController.textFields.firstObject).text;
                                   
                                   NSString *password = ((UITextField *)alertController.textFields[1]).text;
                                   
                                   NSLog(@"Email address entered: %@", email);
                                   
                                   if(email.length > 0 && password.length > 0){
                                       
                                       if(password.length > 5){
                                           
                                           [backendless.userService login:email password:password response:^(BackendlessUser *user) {
                                               [backendless.userService setStayLoggedIn:YES];
                                               [alertController dismissViewControllerAnimated:YES completion:nil];
                                               [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"createdAccount"];
                                               [[NSUserDefaults standardUserDefaults] synchronize];
                                               if([[NSUserDefaults standardUserDefaults] boolForKey:@"trackerDone"]){
                                                   
                                                   [self showSkipAlert];
                                               }

                                               
                                           } error:^(Fault *fault) {
                                               
                                           }];
                                           
                                       }
                                       else{
                                           
                                           
                                       }
                                       
                                   }
                                   else{
                                       
                                   }
                               }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alertController dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    
    [alertController addAction:cancel];
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}



-(void) registerUser {
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Register"
                                          message:@"Please create a free one-time login account, or login, so you can access your challenge and journal on any of your devices."
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"email";
     }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"password";
     }];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"Register"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   NSString *email = ((UITextField *)alertController.textFields.firstObject).text;
                                   
                                   NSString *password = ((UITextField *)alertController.textFields[1]).text;
                                   
                                   NSLog(@"Email address entered: %@", email);
                                   
                                   if(email.length > 0 && password.length > 0){
                                       
                                       if(password.length > 5){
                                           
                                           BackendlessUser *user = [BackendlessUser new];
                                           user.password = password;
                                           user.email = email;
                                           
                                           
                                           // non-blocking method
                                           [backendless.userService registerUser:user response:^(BackendlessUser *user) {
                                               
                                               [backendless.userService login:email password:password response:^(BackendlessUser *user) {
                                                   [backendless.userService setStayLoggedIn:YES];
                                                   
                                                   
                                                   [alertController dismissViewControllerAnimated:YES completion:nil];
                                                   [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"createdAccount"];
                                                   [[NSUserDefaults standardUserDefaults] synchronize];
                                                   if([[NSUserDefaults standardUserDefaults] boolForKey:@"trackerDone"]){
                                                       
                                                       [self showSkipAlert];
                                                   }
                                                   
                                                   
                                               } error:^(Fault *fault) {
                                                   
                                               }];
                                               
                                           } error:^(Fault *error) {
                                               
                                           }];
                                           
                                           /*[backendless.userService registering:user response:^(BackendlessUser *response) {
                                               
                                               
                                               [backendless.userService login:email password:password response:^(BackendlessUser *user) {
                                                   [backendless.userService setStayLoggedIn:YES];
                                                  
                                                 
                                                   [alertController dismissViewControllerAnimated:YES completion:nil];
                                                   [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"createdAccount"];
                                                   [[NSUserDefaults standardUserDefaults] synchronize];
                                                   if([[NSUserDefaults standardUserDefaults] boolForKey:@"trackerDone"]){
                                                       
                                                       [self showSkipAlert];
                                                   }
                                                 
                                                   
                                               } error:^(Fault *fault) {
                                                   
                                               }];
                                               
                                           } error:^(Fault *fault) {
                                               
                                           }];*/
                                           
                                       }
                                       else{
                                           
                                       }
                                       
                                   }
                                   else{
                                       
                                   }
                               }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alertController dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    UIAlertAction* login = [UIAlertAction
                            actionWithTitle:@"Have an account?"
                            style:UIAlertActionStyleDefault
                            handler:^(UIAlertAction * action)
                            {
                                
                                
                                [alertController dismissViewControllerAnimated:YES completion:^{
                                    
                                    
                                }];
                                [self loginUser];
                                
                            }];
    
    
    
    [alertController addAction:okAction];
    [alertController addAction:login];
    [alertController addAction:cancel];
    
    
    
    [self presentViewController:alertController animated:YES completion:nil];
    
    
}

-(void) viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
     [self.scrollView setContentSize:CGSizeMake(self.view.frame.size.width, 750)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)submitClicked:(id)sender {
    
    if(self.hearAboutTextField.text.length > 0 && self.impactOnLifeTextField.text.length > 0 && self.dioceseTextField.text.length && self.countryTextField.text.length && self.howManyForgivenTextField.text.length > 0 && self.hurtsHealed.text.length > 0 && self.howManyChallengedTextField.text.length >  0 && self.feedbackTextField.text.length > 0){
    MercyTracker *tracker = [[MercyTracker alloc] init];
    tracker.hearAbout = self.hearAboutTextField.text;
    tracker.impactOnLife = self.impactOnLifeTextField.text;
    tracker.diocese = self.dioceseTextField.text;
    tracker.country = self.countryTextField.text;
    tracker.howManyForgiven = self.howManyForgivenTextField.text;
    tracker.hurtsHealed = self.hurtsHealed.text;
    tracker.howManyChallenged = self.howManyChallengedTextField.text;
    tracker.feedback = self.feedbackTextField.text;
    tracker.userId = backendless.userService.currentUser.objectId;
    
    id<IDataStore> dataStore = [backendless.data of:[MercyTracker class]];
    [dataStore save:tracker response:^(id response) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"trackerDone"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self goToTestimonialView];
        });
        
    } error:^(Fault *fault) {
        
        OpinionzAlertView *alertView = [[OpinionzAlertView alloc] initWithTitle:@"Error" message: @"There was an error saving your Tracker." cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [alertView setIconType:OpinionzAlertIconWarning];
        [alertView show];

    }];
    }
    else{
       
        OpinionzAlertView *alertView = [[OpinionzAlertView alloc] initWithTitle:@"Missing Information" message: @"Please make sure all fields are fille out before submitting." cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [alertView setIconType:OpinionzAlertIconWarning];
        [alertView show];

        
    }


}

-(void) setUpBackgroundImage{
    
    UIImageView *background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [background setImage:[UIImage imageNamed:@"sail8"]];
    [background setContentMode:UIViewContentModeScaleAspectFill];
    [background setAlpha:0.75];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [view setBackgroundColor:[UIColor blackColor]];
    [view setAlpha: 0.3];
    [self.view addSubview:view];
    [self.view sendSubviewToBack:view];
    [self.view addSubview:background];
    [self.view sendSubviewToBack:background];
}

-(void) openRightMenu {
    
    AppDelegate *appDelegate = [AppDelegate sharedInstanceofAppdelegate];
    [appDelegate.sideMenuViewController presentRightMenuViewController];
    
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"HighlightChapter" object:[NSNumber numberWithInt:(int)[appDelegate.book.chapters count] - 1] userInfo:nil];
}

-(void) goToChapter: (NSNotification *) notification{
    
    NSArray *array = [[notification object] componentsSeparatedByString:@","];
    NSLog(@"Array: %@", array);
    __block int counter = [array[0] intValue];
    
    AppDelegate *appDelegate = [AppDelegate sharedInstanceofAppdelegate];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"HomePageStoryboard" bundle:nil];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[storyboard instantiateViewControllerWithIdentifier:@"homePage"]];
    
    [appDelegate.sideMenuViewController setContentViewController:navigationController animated:YES];
    
    [appDelegate.sideMenuViewController hideMenuViewController];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"GoToChapter" object:[NSNumber numberWithInt:counter] userInfo:nil];
    });
}



- (IBAction)skipButtonClicked:(id)sender {
}
@end
