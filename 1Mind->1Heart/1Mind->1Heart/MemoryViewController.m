//
//  MemoryViewController.m
//  Mercy Challenge
//
//  Created by Taylor Korensky on 4/24/16.
//  Copyright © 2016 Appsky Labs. All rights reserved.
//

#import "MemoryViewController.h"

@interface MemoryViewController ()

@end

@implementation MemoryViewController

-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if(self.typeId == EDIT){
        self.personTextField.text = self.memory.person;
        self.eventTextField.text = self.memory.event;
    }
    
  }

-(void) viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    [self checkRegisterStuff];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) checkRegisterStuff {
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"createdAccount"]){ //have an account already
        
        if([backendless.userService currentUser] == nil){
            
            [self loginUser];
        }
        else{ //normal launch
            
            
        }
    }
    else{  //dont have an account yet
        
        [self registerUser];
        
    }
    
}


-(void) loginUser{
    
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Login"
                                          message:@"Please login to your Mercy Challenge account."
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"email";
     }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"password";
     }];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   NSString *email = ((UITextField *)alertController.textFields.firstObject).text;
                                   
                                   NSString *password = ((UITextField *)alertController.textFields[1]).text;
                                   
                                   NSLog(@"Email address entered: %@", email);
                                   
                                   if(email.length > 0 && password.length > 0){
                                       
                                       if(password.length > 5){
                                           
                                           [backendless.userService login:email password:password response:^(BackendlessUser *user) {
                                               [backendless.userService setStayLoggedIn:YES];
                                               [alertController dismissViewControllerAnimated:YES completion:nil];
                                               [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"createdAccount"];
                                               [[NSUserDefaults standardUserDefaults] synchronize];
                                               
                                           } error:^(Fault *fault) {
                                               
                                               OpinionzAlertView *alertView = [[OpinionzAlertView alloc] initWithTitle:@"Error" message: @"There was an error with the login." cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                                               [alertView setIconType:OpinionzAlertIconWarning];
                                               [alertView show];
                                               
                                               
                                               [self performSelector:@selector(checkRegisterStuff) withObject:nil afterDelay:1.00];
                                               
                                           }];
                                           
                                       }
                                       else{
                                           [alertController dismissViewControllerAnimated:YES completion:nil];
                                           OpinionzAlertView *alertView = [[OpinionzAlertView alloc] initWithTitle:@"Error" message: @"Make sure password is at least 6 characters." cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                                           [alertView setIconType:OpinionzAlertIconWarning];
                                           [alertView show];
                                           [self performSelector:@selector(checkRegisterStuff) withObject:nil afterDelay:1.00];
                                           
                                       }
                                       
                                   }
                                   else{
                                       
                                       [alertController dismissViewControllerAnimated:YES completion:nil];
                                       OpinionzAlertView *alertView = [[OpinionzAlertView alloc] initWithTitle:@"Error" message: @"Make sure all fields are filled out." cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                                       [alertView setIconType:OpinionzAlertIconWarning];
                                       [alertView show];
                                       [self performSelector:@selector(checkRegisterStuff) withObject:nil afterDelay:1.00];
                                       
                                   }
                               }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alertController dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    
    [alertController addAction:cancel];
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}



-(void) registerUser {
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Register"
                                          message:@"Please create a free one-time login account, or login, so you can access your challenge and journal on any of your devices."
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"email";
     }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"password";
     }];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"Register"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   NSString *email = ((UITextField *)alertController.textFields.firstObject).text;
                                   
                                   NSString *password = ((UITextField *)alertController.textFields[1]).text;
                                   
                                   NSLog(@"Email address entered: %@", email);
                                   
                                   if(email.length > 0 && password.length > 0){
                                       
                                       if(password.length > 5){
                                           
                                           BackendlessUser *user = [BackendlessUser new];
                                           user.password = password;
                                           user.email = email;
                                           
                                           [backendless.userService registerUser:user response:^(BackendlessUser *response) {
                                               
                                               
                                               [backendless.userService login:email password:password response:^(BackendlessUser *user) {
                                                   [backendless.userService setStayLoggedIn:YES];
                                                   [alertController dismissViewControllerAnimated:YES completion:nil];
                                                   [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"createdAccount"];
                                                   [[NSUserDefaults standardUserDefaults] synchronize];
                                                   
                                               } error:^(Fault *fault) {
                                                   
                                                   OpinionzAlertView *alertView = [[OpinionzAlertView alloc] initWithTitle:@"Error" message: @"Error with the login." cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                                                   [alertView setIconType:OpinionzAlertIconWarning];
                                                   [alertView show];
                                                   
                                                   
                                                   [self performSelector:@selector(checkRegisterStuff) withObject:nil afterDelay:1.00];
                                                   
                                               }];
                                               
                                           } error:^(Fault *fault) {
                                               
                                               OpinionzAlertView *alertView = [[OpinionzAlertView alloc] initWithTitle:@"Error" message: @"Error with the registration." cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                                               [alertView setIconType:OpinionzAlertIconWarning];
                                               [alertView show];
                                               
                                               
                                               [self performSelector:@selector(checkRegisterStuff) withObject:nil afterDelay:1.00];
                                           }];
                                           
                                       }
                                       else{
                                           
                                           [alertController dismissViewControllerAnimated:YES completion:nil];
                                           OpinionzAlertView *alertView = [[OpinionzAlertView alloc] initWithTitle:@"Error" message: @"Make sure password is at least 6 characters." cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                                           [alertView setIconType:OpinionzAlertIconWarning];
                                           [alertView show];
                                           [self performSelector:@selector(checkRegisterStuff) withObject:nil afterDelay:1.00];
                                       }
                                       
                                   }
                                   else{
                                       
                                       [alertController dismissViewControllerAnimated:YES completion:nil];
                                       OpinionzAlertView *alertView = [[OpinionzAlertView alloc] initWithTitle:@"Error" message: @"Make sure all fields are filled out." cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                                       [alertView setIconType:OpinionzAlertIconWarning];
                                       [alertView show];
                                       [self performSelector:@selector(checkRegisterStuff) withObject:nil afterDelay:1.00];
                                   }
                               }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alertController dismissViewControllerAnimated:YES completion:nil];
                                 [self.navigationController popViewControllerAnimated:YES];
                                 
                             }];
    UIAlertAction* login = [UIAlertAction
                            actionWithTitle:@"Have an account?"
                            style:UIAlertActionStyleDefault
                            handler:^(UIAlertAction * action)
                            {
                                
                                
                                [alertController dismissViewControllerAnimated:YES completion:^{
                                    
                                    
                                }];
                                [self loginUser];
                                
                            }];
    
    
    
    [alertController addAction:okAction];
    [alertController addAction:login];
    [alertController addAction:cancel];
    
    
    
    [self presentViewController:alertController animated:YES completion:nil];
    
    
}

- (IBAction)saveMemoryClicked:(id)sender {
    
    if(self.eventTextField.text.length > 0){
        
        if(self.memory == nil){
            self.memory = [[Memory alloc] init];
        }
        NSLog(@"Memory: %@", self.memory.objectId);
        self.memory.person = self.personTextField.text;
        self.memory.event = self.eventTextField.text;
        self.memory.userId = [[backendless.userService currentUser] getObjectId];
        self.memory.rank1 = [NSNumber numberWithInt:[self.rank1Label.text intValue]];
        self.memory.rank2 = [NSNumber numberWithInt:[self.rank2Label.text intValue]];
        self.memory.rank3 = [NSNumber numberWithInt:[self.rank3Label.text intValue]];
        if(self.forgivenSegmentViewController.selectedSegmentIndex == 0){
            self.memory.forgiven = [NSNumber numberWithBool:YES];
        }else{
            self.memory.forgiven = [NSNumber numberWithBool:NO];
        }
        if(self.memoryWorkSegmentController.selectedSegmentIndex == 0){
            self.memory.memoryWork = [NSNumber numberWithBool:YES];
        }else{
            self.memory.memoryWork = [NSNumber numberWithBool:NO];
        }
        int currentRanking =[self.memory.currentRanking intValue];
        
        if((currentRanking < 3 && self.rankUp == YES) || (currentRanking == 0 && [self.rank1Label.text intValue] == 10)){
            currentRanking++;
            self.memory.currentRanking = [NSNumber numberWithInt:currentRanking];
        }
        
        id<IDataStore> dataStore = [backendless.data of:[Memory class]];
        [dataStore save:self.memory response:^(id response) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:YES];
            });
        
        } error:^(Fault *fault) {
            
            OpinionzAlertView *alertView = [[OpinionzAlertView alloc] initWithTitle:@"Error" message: @"There was an error saving your memory." cancelButtonTitle:@"Okay" otherButtonTitles:nil];
            [alertView setIconType:OpinionzAlertIconWarning];
            [alertView show];

        }];
    }
    else{
        
        OpinionzAlertView *alertView = [[OpinionzAlertView alloc] initWithTitle:@"Missing Information" message: @"Please make sure you have each field filled out." cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [alertView setIconType:OpinionzAlertIconWarning];
        [alertView show];

    }
}
- (IBAction)valueChanged:(id)sender {
    
    if(sender == self.rank1Stepper){
        
        [self.rank1Label setText:[NSString stringWithFormat:@"%d", (int)[self.rank1Stepper value]]];
        self.rankUp = YES;
    }
    else if(sender == self.rank2Stepper){
        
        [self.rank2Label setText:[NSString stringWithFormat:@"%d", (int)[self.rank2Stepper value]]];
         self.rankUp = YES;
    }
    else if(sender == self.rank3Stepper){
        
        [self.rank3Label setText:[NSString stringWithFormat:@"%d", (int)[self.rank3Stepper value]]];
         self.rankUp = YES;
    }
}

-(void) setUpView: (Memory *)memory{

    self.memory = memory;
    NSLog(@"Memory: %@", self.memory);
    
    if(self.memory != nil){
        self.typeId = EDIT;
    }
    else{
       self.typeId = CREATE;
    }
}

- (IBAction)keyboardNext:(id)sender {
    
    if(sender == self.personTextField){
        [self.personTextField resignFirstResponder];
        [self.eventTextField becomeFirstResponder];
    }else{
        [self.eventTextField becomeFirstResponder];
    }
}



@end
