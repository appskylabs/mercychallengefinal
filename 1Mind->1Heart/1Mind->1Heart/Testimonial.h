#import <Foundation/Foundation.h>

@interface Testimonial : NSObject
@property (nonatomic, strong) NSDate *created;
@property (nonatomic, strong) NSString *testimonial;
@property (nonatomic, strong) NSString *ownerId;
@property (nonatomic, strong) NSString *objectId;
@property (nonatomic, strong) NSDate *updated;
@property (nonatomic, strong) NSNumber *allowShare;
@end