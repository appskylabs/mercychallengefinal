//
//  ViewController.m
//  1Mind->1Heart
//
//  Created by Taylor Korensky on 3/26/16.
//  Copyright © 2016 Appsky Labs. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

-(void ) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.backgroundGIF.gifPath = [[NSBundle mainBundle] pathForResource:@"fire.gif" ofType:nil];
        [self.backgroundGIF startGIF];
        
    });
    self.loginOrSignUp = LOGIN;
    
    [self.emailTextField setHidden: YES];
    [self.passwordTextField setHidden: YES];
     [self.iconImageView setHidden:YES];
    [self.signUpButton setHidden:YES];
        [self.loginSignUpButton setHidden:YES];
    
    [self.emailSignUpTextField setHidden:YES];
    [self.firstNameTextField setHidden:YES];
    [self.lastNameTextField setHidden:YES];
    [self.passwordSignUpTextField setHidden:YES];
    [self.passwordVerifySignUpTextField setHidden:YES];
    [self.resetPasswordButton setHidden:YES];
    
    [self setUpSignInView];
    [self setUpSignUpView];
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void) setUpSignInView {
    

    self.emailTextField.textColor=[UIColor whiteColor];
    self.emailTextField.enableMaterialPlaceHolder = YES;
    self.emailTextField.errorColor=[UIColor colorWithRed:0.910 green:0.329 blue:0.271 alpha:1.000]; // FLAT RED COLOR
    self.emailTextField.lineColor=[UIColor colorWithRed:0.9569 green:0.9569 blue:0.9569 alpha:1.0];
    self.emailTextField.tintColor=[UIColor colorWithRed:1.0 green:0.6314 blue:0.1658 alpha:0.951013513513513];
    self.emailTextField.placeholder=@"email";
    self.emailTextField.placeholderAttributes = @{NSFontAttributeName : [UIFont systemFontOfSize:20],
                                                  NSForegroundColorAttributeName : [[UIColor colorWithRed:0.9569 green:0.9569 blue:0.9569 alpha:1.0] colorWithAlphaComponent:.8]};
    
    self.passwordTextField.textColor=[UIColor whiteColor];
    self.passwordTextField.enableMaterialPlaceHolder = YES;
    self.passwordTextField.errorColor=[UIColor colorWithRed:0.910 green:0.329 blue:0.271 alpha:1.000]; // FLAT RED COLOR
    self.passwordTextField.lineColor=[UIColor colorWithRed:0.9569 green:0.9569 blue:0.9569 alpha:1.0];
    self.passwordTextField.tintColor=[UIColor colorWithRed:1.0 green:0.6314 blue:0.1658 alpha:0.951013513513513];
    self.passwordTextField.placeholder=@"password";
    self.passwordTextField.placeholderAttributes = @{NSFontAttributeName : [UIFont systemFontOfSize:20],
                                                     NSForegroundColorAttributeName : [[UIColor colorWithRed:0.9569 green:0.9569 blue:0.9569 alpha:1.0] colorWithAlphaComponent:.8]};
    
    [self performSelector:@selector(showAfterDelay) withObject:nil afterDelay:0.75];
    
}

-(void) showAfterDelay {
    
    [self.loginSignUpButton setHidden:NO];
      [self.signUpButton setHidden:NO];
    [self.iconImageView setHidden:NO];
    [self.emailTextField setHidden: NO];
    [self.passwordTextField setHidden: NO];
    [self.resetPasswordButton setHidden:NO];
}


-(void) setUpSignUpView {
    
    self.emailSignUpTextField.textColor=[UIColor whiteColor];
    self.emailSignUpTextField.enableMaterialPlaceHolder = YES;
    self.emailSignUpTextField.errorColor=[UIColor colorWithRed:0.910 green:0.329 blue:0.271 alpha:1.000]; // FLAT RED COLOR
    self.emailSignUpTextField.lineColor=[UIColor colorWithRed:0.9569 green:0.9569 blue:0.9569 alpha:1.0];
    self.emailSignUpTextField.tintColor=[UIColor colorWithRed:1.0 green:0.6314 blue:0.1658 alpha:0.951013513513513];
    self.emailSignUpTextField.placeholder=@"email";
    self.emailSignUpTextField.placeholderAttributes = @{NSFontAttributeName : [UIFont systemFontOfSize:20],
                                                  NSForegroundColorAttributeName : [[UIColor colorWithRed:0.9569 green:0.9569 blue:0.9569 alpha:1.0] colorWithAlphaComponent:.8]};
    
    self.firstNameTextField.textColor=[UIColor whiteColor];
    self.firstNameTextField.enableMaterialPlaceHolder = YES;
    self.firstNameTextField.errorColor=[UIColor colorWithRed:0.910 green:0.329 blue:0.271 alpha:1.000]; // FLAT RED COLOR
    self.firstNameTextField.lineColor=[UIColor colorWithRed:0.9569 green:0.9569 blue:0.9569 alpha:1.0];
    self.firstNameTextField.tintColor=[UIColor colorWithRed:1.0 green:0.6314 blue:0.1658 alpha:0.951013513513513];
    self.firstNameTextField.placeholder=@"first name";
    self.firstNameTextField.placeholderAttributes = @{NSFontAttributeName : [UIFont systemFontOfSize:20],
                                                        NSForegroundColorAttributeName : [[UIColor colorWithRed:0.9569 green:0.9569 blue:0.9569 alpha:1.0] colorWithAlphaComponent:.8]};
    
    self.lastNameTextField.textColor=[UIColor whiteColor];
    self.lastNameTextField.enableMaterialPlaceHolder = YES;
    self.lastNameTextField.errorColor=[UIColor colorWithRed:0.910 green:0.329 blue:0.271 alpha:1.000]; // FLAT RED COLOR
    self.lastNameTextField.lineColor=[UIColor colorWithRed:0.9569 green:0.9569 blue:0.9569 alpha:1.0];
    self.lastNameTextField.tintColor=[UIColor colorWithRed:1.0 green:0.6314 blue:0.1658 alpha:0.951013513513513];
    self.lastNameTextField.placeholder=@"last name";
    self.lastNameTextField.placeholderAttributes = @{NSFontAttributeName : [UIFont systemFontOfSize:20],
                                                      NSForegroundColorAttributeName : [[UIColor colorWithRed:0.9569 green:0.9569 blue:0.9569 alpha:1.0] colorWithAlphaComponent:.8]};
    
    self.passwordSignUpTextField.textColor=[UIColor whiteColor];
    self.passwordSignUpTextField.enableMaterialPlaceHolder = YES;
    self.passwordSignUpTextField.errorColor=[UIColor colorWithRed:0.910 green:0.329 blue:0.271 alpha:1.000]; // FLAT RED COLOR
    self.passwordSignUpTextField.lineColor=[UIColor colorWithRed:0.9569 green:0.9569 blue:0.9569 alpha:1.0];
    self.passwordSignUpTextField.tintColor=[UIColor colorWithRed:1.0 green:0.6314 blue:0.1658 alpha:0.951013513513513];
    self.passwordSignUpTextField.placeholder = @"password (6 characters)";
    self.passwordSignUpTextField.placeholderAttributes = @{NSFontAttributeName : [UIFont systemFontOfSize:20],
                                                     NSForegroundColorAttributeName : [[UIColor colorWithRed:0.9569 green:0.9569 blue:0.9569 alpha:1.0] colorWithAlphaComponent:.8]};
    
    self.passwordVerifySignUpTextField.textColor=[UIColor whiteColor];
    self.passwordVerifySignUpTextField.enableMaterialPlaceHolder = YES;
    self.passwordVerifySignUpTextField.errorColor=[UIColor colorWithRed:0.910 green:0.329 blue:0.271 alpha:1.000]; // FLAT RED COLOR
    self.passwordVerifySignUpTextField.lineColor=[UIColor colorWithRed:0.9569 green:0.9569 blue:0.9569 alpha:1.0];
    self.passwordVerifySignUpTextField.tintColor=[UIColor colorWithRed:1.0 green:0.6314 blue:0.1658 alpha:0.951013513513513];
    self.passwordVerifySignUpTextField.placeholder = @"verify password";
    self.passwordVerifySignUpTextField.placeholderAttributes = @{NSFontAttributeName : [UIFont systemFontOfSize:20],
                                                           NSForegroundColorAttributeName : [[UIColor colorWithRed:0.9569 green:0.9569 blue:0.9569 alpha:1.0] colorWithAlphaComponent:.8]};
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void) viewDidAppear:(BOOL)animated{
    
   /* if([[NSUserDefaults standardUserDefaults] boolForKey:@"loggedIn"]){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"HomePageStoryboard" bundle:nil];
            UIHomePageViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"homePage"];
            [self presentViewController:viewController animated:YES completion:nil];
        });
    }*/

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)keyboardNext:(id)sender {
    
    if(sender == self.emailTextField){
        [self.emailTextField resignFirstResponder];
        [self.passwordTextField becomeFirstResponder];
    }
    else if(sender == self.passwordTextField){
        [self.passwordTextField resignFirstResponder];
    }
    else if(sender == self.emailSignUpTextField){
        [self.emailSignUpTextField resignFirstResponder];
        [self.firstNameTextField becomeFirstResponder];
    }
    else if(sender == self.firstNameTextField){
        [self.firstNameTextField resignFirstResponder];
        [self.lastNameTextField becomeFirstResponder];
    }
    else if(sender == self.lastNameTextField){
        [self.lastNameTextField resignFirstResponder];
        [self.passwordSignUpTextField becomeFirstResponder];
    }
    else if(sender == self.passwordSignUpTextField){
        [self.passwordSignUpTextField resignFirstResponder];
        [self.passwordVerifySignUpTextField becomeFirstResponder];
    }
    else if(sender == self.passwordVerifySignUpTextField){
        [self.passwordVerifySignUpTextField resignFirstResponder];
    }
}
- (IBAction)signUpClicked:(id)sender {
    
    if(self.loginOrSignUp == LOGIN){
        
        [self.iconImageView setHidden:YES];
        [self.emailSignUpTextField setHidden:NO];
        [self.firstNameTextField setHidden:NO];
        [self.lastNameTextField setHidden:NO];
        [self.passwordSignUpTextField setHidden:NO];
        [self.passwordVerifySignUpTextField setHidden:NO];
        [self.resetPasswordButton setHidden:YES];
        
        [self.emailTextField setHidden:YES];
        [self.passwordTextField setHidden:YES];
        
        [self.loginSignUpButton setTitle:@"Sign Up" forState:UIControlStateNormal];
        [self.signUpButton setTitle:@"Cancel" forState:UIControlStateNormal];
        self.loginOrSignUp = SIGNUP;
    
    }
    else{
    
        [self.emailSignUpTextField setHidden:YES];
        [self.firstNameTextField setHidden:YES];
        [self.lastNameTextField setHidden:YES];
        [self.passwordSignUpTextField setHidden:YES];
        [self.passwordVerifySignUpTextField setHidden:YES];
        [self.resetPasswordButton setHidden:NO];
        [self.emailTextField setHidden:NO];
        [self.passwordTextField setHidden:NO];
        
        [self.loginSignUpButton setTitle:@"Log In" forState:UIControlStateNormal];
        [self.signUpButton setTitle:@"Sign Up" forState:UIControlStateNormal];
         self.loginOrSignUp = LOGIN;
        [self.iconImageView setHidden:NO];
    }
}

- (IBAction)loginSignUpButtonClicked:(id)sender {
    
    if(self.loginOrSignUp == LOGIN){
        
        if(self.emailTextField.text.length > 0 && self.passwordTextField.text.length > 5){
            //Login the user
              [SVProgressHUD show];
            [backendless.userService login:self.emailTextField.text password:self.passwordTextField.text  response:^(BackendlessUser *loggedInUser) {
                NSLog(@"Login Success!");
                
                [backendless.userService setStayLoggedIn:YES];
                [SVProgressHUD dismiss];
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"loggedIn"];
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"HomePageStoryboard" bundle:nil];
                UIHomePageViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"homePage"];
                [self presentViewController:viewController animated:YES completion:nil];
                
                
            } error:^(Fault *fault) {
                
                NSLog(@"Failed to Login: %@", fault.message);
                [SVProgressHUD dismiss];
                OpinionzAlertView *alertView = [[OpinionzAlertView alloc] initWithTitle:@"Error" message: fault.message cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                [alertView setIconType:OpinionzAlertIconWarning];
                [alertView show];
            }];

            
        }
        else{ //fill out all forms
            
            OpinionzAlertView *alertView = [[OpinionzAlertView alloc] initWithTitle:@"Missing Information" message: @"Please make sure you have the email and password fields filled in." cancelButtonTitle:@"Okay" otherButtonTitles:nil];
            [alertView setIconType:OpinionzAlertIconWarning];
            [alertView show];

        }
    
    }
    else{
        if(self.emailSignUpTextField.text.length > 0 && self.firstNameTextField.text.length > 0 && self.lastNameTextField.text.length > 0 && self.passwordSignUpTextField.text.length > 0 && self.passwordVerifySignUpTextField.text.length > 0){
            
            if([self.passwordSignUpTextField.text isEqualToString:self.passwordVerifySignUpTextField.text]){
                
                [SVProgressHUD show];
                //Sign up the user
                BackendlessUser *user = [BackendlessUser new];
                user.email = self.emailSignUpTextField.text;
                user.password = self.passwordSignUpTextField.text;
                [user setProperty:@"firstname" object:self.firstNameTextField.text];
                [user setProperty:@"lastname" object:self.lastNameTextField.text];
                [user setProperty: @"appIdentifier" object:[[NSBundle mainBundle] bundleIdentifier]];
                
                [backendless.userService registerUser:user response:^(BackendlessUser *user) {
                
                    NSLog(@"Registered User: %@", user);
                    [SVProgressHUD dismiss];
                    OpinionzAlertView *alertView = [[OpinionzAlertView alloc] initWithTitle:@"Verify Email" message:@"An email has been sent with an account verification link. You must verify your account before logging in for the first time." cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alertView setIconType:OpinionzAlertIconInfo];
                    [alertView show];
                    
                } error:^(Fault *fault) {
                   
                    NSLog(@"Fault: %@", fault.message);
                    [SVProgressHUD dismiss];
                    OpinionzAlertView *alertView = [[OpinionzAlertView alloc] initWithTitle:@"Error" message: fault.message cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alertView setIconType:OpinionzAlertIconWarning];
                    [alertView show];
                }];
                
                //old version of backendless
               /* [backendless.userService registering:user response:^(BackendlessUser *registeredUser) {
                    
                    NSLog(@"Registered User: %@", user);
                    [SVProgressHUD dismiss];
                    OpinionzAlertView *alertView = [[OpinionzAlertView alloc] initWithTitle:@"Verify Email" message:@"An email has been sent with an account verification link. You must verify your account before logging in for the first time." cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alertView setIconType:OpinionzAlertIconInfo];
                    [alertView show];
                    
                } error:^(Fault *fault) {
                    NSLog(@"Fault: %@", fault.message);
                    [SVProgressHUD dismiss];
                    OpinionzAlertView *alertView = [[OpinionzAlertView alloc] initWithTitle:@"Error" message: fault.message cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                    [alertView setIconType:OpinionzAlertIconWarning];
                    [alertView show];
                    
                    
                }];*/

            }
            else{  //Passwords dont match
                
                OpinionzAlertView *alertView = [[OpinionzAlertView alloc] initWithTitle:@"Password Mismatch" message: @"Please make sure the password fields are the same and at least 6 characters." cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                [alertView setIconType:OpinionzAlertIconWarning];
                [alertView show];
            }
        
        }
        else{ //fill out all forms
            
            OpinionzAlertView *alertView = [[OpinionzAlertView alloc] initWithTitle:@"Missing Information" message: @"Please make sure you have all of the fields filled in." cancelButtonTitle:@"Okay" otherButtonTitles:nil];
            [alertView setIconType:OpinionzAlertIconWarning];
            [alertView show];

        }
        
    }
    
}
- (IBAction)resetPassword:(id)sender {
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Forgot Password"
                                          message:@"Please enter your email."
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"email";
     }];
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [SVProgressHUD show];
                                   NSString *email = ((UITextField *)alertController.textFields.firstObject).text;
                                   
                                   NSLog(@"Email address entered: %@", email);
                                   
                                   if(email.length > 0){
                                       
                                       
                                       [backendless.userService restorePassword:email response:^(id response) {
                                           [SVProgressHUD dismiss];
                                            [alertController dismissViewControllerAnimated:YES completion:nil];
                                           OpinionzAlertView *alertView = [[OpinionzAlertView alloc] initWithTitle:@"Success" message:@"An email has been sent to you containing instructions on how to reset your password." cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                                           [alertView setIconType:OpinionzAlertIconSuccess];
                                           [alertView show];
                                           
                                       } error:^(Fault *fault) {
                                          [SVProgressHUD dismiss];
                                           OpinionzAlertView *alertView = [[OpinionzAlertView alloc] initWithTitle:@"Error" message: fault.message cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                                           [alertView setIconType:OpinionzAlertIconWarning];
                                           [alertView show];
                                           
                                       }];
                                                                          }
                                   else{
                                        [alertController dismissViewControllerAnimated:YES completion:nil];
                                       [SVProgressHUD dismiss];
                                       //[BFAlertControllerHelper showErrorAlertWithViewController:self message:@"Please fill out the field"];
                                       OpinionzAlertView *alertView = [[OpinionzAlertView alloc] initWithTitle:@"Missing Information" message: @"Please make sure you have the field filled out." cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                                       [alertView setIconType:OpinionzAlertIconWarning];
                                       [alertView show];

                                   }
                               }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                
                                 
                             }];
    
    
    [alertController addAction:cancel];
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)dismissKeyboard:(id)sender {
    
    [self.emailTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    [self.emailSignUpTextField resignFirstResponder];
    [self.firstNameTextField resignFirstResponder];
    [self.lastNameTextField resignFirstResponder];
    [self.passwordSignUpTextField resignFirstResponder];
    [self.passwordVerifySignUpTextField resignFirstResponder];
}
@end
