//
//  LeftMenuViewController.h
//  
//
//  Created by Taylor Korensky on 4/21/16.
//
//

#import "Book.h"
#import "AppDelegate.h"
#import <UIKit/UIKit.h>
#import "UIHomePageViewController.h"
#import "DonateViewController.h"
#import "ContactUsViewController.h"


@interface LeftMenuViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong,nonatomic) Book *book;
@property (strong, nonatomic) NSMutableArray *titlesArray;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) RESideMenu *sideMenu;

@end
