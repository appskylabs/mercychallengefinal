//
//  Reader.swift
//  1Mind->1Heart
//
//  Created by Taylor Korensky on 3/28/16.
//  Copyright © 2016 Appsky Labs. All rights reserved.
//


import Foundation
import FolioReaderKit

func open(sender: AnyObject) {
    let config = FolioReaderConfig()
    let bookPath = NSBundle.mainBundle().pathForResource("book", ofType: "epub")
    FolioReader.presentReader(parentViewController: self, withEpubPath: bookPath!, andConfig: config)
}