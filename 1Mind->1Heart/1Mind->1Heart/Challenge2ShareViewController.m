//
//  Challenge2ShareViewController.m
//  Mercy Challenge
//
//  Created by Taylor Korensky on 5/2/16.
//  Copyright © 2016 Appsky Labs. All rights reserved.
//

#import "Challenge2ShareViewController.h"

@interface Challenge2ShareViewController ()

@end

@implementation Challenge2ShareViewController

-(void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self setUpBackgroundImage];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIButton *aButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [aButton setBackgroundImage: [UIImage imageNamed: @"MenuSettings"] forState:UIControlStateNormal];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:aButton];
    [aButton addTarget:self action:@selector(openMenu) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = backButton;
    
    UIButton *rightButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [rightButton setBackgroundImage: [UIImage imageNamed: @"BookIcon"] forState:UIControlStateNormal];
    UIBarButtonItem *bookButton = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    [rightButton addTarget:self action:@selector(openRightMenu) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = bookButton;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goToChapter:) name:@"GoToChapterFromOtherView" object:nil];


}

-(void) openMenu{
    
    AppDelegate *appDelegate = [AppDelegate sharedInstanceofAppdelegate];
    [appDelegate.sideMenuViewController presentLeftMenuViewController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) setUpBackgroundImage{
    
    UIImageView *background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [background setImage:[UIImage imageNamed:@"sail8"]];
    [background setContentMode:UIViewContentModeScaleAspectFill];
    [background setAlpha:0.75];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [view setBackgroundColor:[UIColor blackColor]];
    [view setAlpha: 0.3];
    [self.view addSubview:view];
    [self.view sendSubviewToBack:view];
    [self.view addSubview:background];
    [self.view sendSubviewToBack:background];
}


- (IBAction)shareWithFacebook:(id)sender {
    
    SLComposeViewController *composer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
   // [composer setInitialText:string];
    [self presentViewController:composer animated:YES completion:nil];
}

- (IBAction)shareWithTwitter:(id)sender {
    
    NSString *string = @"I completed the Mercy Challenge, you should check it out! #mercychallenge @CatholicCounslr";
    SLComposeViewController *composer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    [composer setInitialText:string];
    [self presentViewController:composer animated:YES completion:nil];
}

- (IBAction)shareWithEmail:(id)sender {
    
    if ([MFMailComposeViewController canSendMail])
    {
        
        MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
        
        mail.mailComposeDelegate=self;
        
        [mail setSubject:@"I completed the Mercy Challenge, you should check it out!"];
        
        [mail setMessageBody:@"http://holyfamilycounseling.org" isHTML:NO];

        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:mail animated:YES completion:NULL];
        });
    }
    else
    {
        
        OpinionzAlertView *alertView = [[OpinionzAlertView alloc] initWithTitle:@"Email Error" message: @"Please set up the email app on your phone and then try again." cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [alertView setIconType:OpinionzAlertIconWarning];
        [alertView show];
    }

}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

-(void) openRightMenu {
    
    AppDelegate *appDelegate = [AppDelegate sharedInstanceofAppdelegate];
    [appDelegate.sideMenuViewController presentRightMenuViewController];
    
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"HighlightChapter" object:[NSNumber numberWithInt:(int)[appDelegate.book.chapters count] - 1] userInfo:nil];
}

-(void) goToChapter: (NSNotification *) notification{
    
    NSArray *array = [[notification object] componentsSeparatedByString:@","];
    NSLog(@"Array: %@", array);
    __block int counter = [array[0] intValue];
    
    AppDelegate *appDelegate = [AppDelegate sharedInstanceofAppdelegate];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"HomePageStoryboard" bundle:nil];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[storyboard instantiateViewControllerWithIdentifier:@"homePage"]];
    
    [appDelegate.sideMenuViewController setContentViewController:navigationController animated:YES];
    
    [appDelegate.sideMenuViewController hideMenuViewController];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"GoToChapter" object:[NSNumber numberWithInt:counter] userInfo:nil];
    });
}



@end
