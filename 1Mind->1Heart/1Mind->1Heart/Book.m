#import "Backendless.h"
#import "Book.h"
#import "Chapter.h"

@implementation Book

-(void)addToChapters:(Chapter *)chapter {

    if (!self.chapters)
        self.chapters = [NSMutableArray array];

    [self.chapters addObject:chapter];
}

-(void)removeFromChapters:(Chapter *)chapter {

    [self.chapters removeObject:chapter];

    if (!self.chapters.count) {
        self.chapters = nil;
    }
}

-(NSMutableArray *)loadChapters {

    //if (!self.chapters)
        
       // [backendless.persistenceService load:self relations:@[@"chapters"]];

    return self.chapters;
}

-(void)freeChapters {

    if (!self.chapters)
        return;

    [self.chapters removeAllObjects];
    self.chapters = nil;
}
@end
